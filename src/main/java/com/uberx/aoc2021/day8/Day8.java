package com.uberx.aoc2021.day8;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day8 {
    public static List<Display> getDisplays(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(entry -> {
                    final String[] tokens = entry.split("\\|");
                    final String signalPatterns = tokens[0].trim();
                    final String outputValue = tokens[1].trim();
                    return new Display(
                            Arrays.stream(signalPatterns.split(" "))
                                    .map(Day8::getAlphabeticallySortedString)
                                    .collect(Collectors.toList()),
                            Arrays.stream(outputValue.split(" "))
                                    .map(Day8::getAlphabeticallySortedString)
                                    .collect(Collectors.toList()));
                })
                .collect(Collectors.toList());
    }

    private static String getAlphabeticallySortedString(final String str) {
        return str.chars()
                .sorted()
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

    /* Part 1 */
    public int numTimesDigits1Or4Or7Or8Appear(final List<Display> displays) {
        return displays.stream()
                .map(Display::getOutputValue)
                .map(outputValue -> outputValue.stream()
                        .filter(digit -> digit.length() == 2
                                || digit.length() == 4
                                || digit.length() == 3
                                || digit.length() == 7)
                        .collect(Collectors.toList()))
                .map(List::size)
                .reduce(0, Integer::sum);
    }

    /* Part 2 */
    public int totalOutputValue(final List<Display> displays) {
        int totalOutputValue = 0;
        for (final Display display : displays) {
            final Map<Integer, List<String>> signalPatternsBySegmentLength = display.getSignalPatterns()
                    .stream()
                    .collect(Collectors.groupingBy(String::length));
            // 1 (segment length = 2)
            final String signalPattern1 = signalPatternsBySegmentLength.get(2).get(0);
            // 4 (segment length = 4)
            final String signalPattern4 = signalPatternsBySegmentLength.get(4).get(0);
            // 7 (segment length = 3)
            final String signalPattern7 = signalPatternsBySegmentLength.get(3).get(0);
            // 8 (segment length = 7)
            final String signalPattern8 = signalPatternsBySegmentLength.get(7).get(0);

            // signal patterns for 2, 3, and 5 have segment length 5
            // 2 (signal pattern 2 U signal pattern 4 = signal pattern 8)
            final String signalPattern2 = signalPatternsBySegmentLength.get(5)
                    .stream()
                    .filter(signalPattern -> union(signalPattern, signalPattern4).equals(signalPattern8))
                    .findAny()
                    .orElseThrow(RuntimeException::new);
            // 3 (signal pattern 3 ∩ signal pattern 1 = signal pattern 1)
            final String signalPattern3 = signalPatternsBySegmentLength.get(5)
                    .stream()
                    .filter(signalPattern -> intersection(signalPattern, signalPattern1).equals(signalPattern1))
                    .findAny()
                    .orElseThrow(RuntimeException::new);
            // 5 (signal patterns with length 5 - signal pattern 2 and 3)
            final String signalPattern5 = signalPatternsBySegmentLength.get(5)
                    .stream()
                    .filter(signalPattern -> !signalPattern.equals(signalPattern2))
                    .filter(signalPattern -> !signalPattern.equals(signalPattern3))
                    .findAny()
                    .orElseThrow(RuntimeException::new);

            // signal patterns for 0, 6, and 9 have segment length 6
            // 6 (signal pattern 6 ∩ signal pattern 1 = length 1)
            final String signalPattern6 = signalPatternsBySegmentLength.get(6)
                    .stream()
                    .filter(signalPattern -> intersection(signalPattern, signalPattern1).length() == 1)
                    .findAny()
                    .orElseThrow(RuntimeException::new);
            // 0 (signal pattern 0 U signal pattern 4 = signal pattern 8)
            final String signalPattern0 = signalPatternsBySegmentLength.get(6)
                    .stream()
                    .filter(signalPattern -> !signalPattern.equals(signalPattern6))
                    .filter(signalPattern -> union(signalPattern, signalPattern4).equals(signalPattern8))
                    .findAny()
                    .orElseThrow(RuntimeException::new);
            // 9 (signal patterns with length 6 - signal pattern 6 and 0)
            final String signalPattern9 = signalPatternsBySegmentLength.get(6)
                    .stream()
                    .filter(signalPattern -> !signalPattern.equals(signalPattern6))
                    .filter(signalPattern -> !signalPattern.equals(signalPattern0))
                    .findAny()
                    .orElseThrow(RuntimeException::new);

            final Map<String, Integer> signalPatternValues = ImmutableMap.<String, Integer>builder()
                    .put(signalPattern0, 0)
                    .put(signalPattern1, 1)
                    .put(signalPattern2, 2)
                    .put(signalPattern3, 3)
                    .put(signalPattern4, 4)
                    .put(signalPattern5, 5)
                    .put(signalPattern6, 6)
                    .put(signalPattern7, 7)
                    .put(signalPattern8, 8)
                    .put(signalPattern9, 9)
                    .build();
            totalOutputValue += Integer.parseInt(
                    display.getOutputValue()
                            .stream()
                            .map(signalPatternValues::get)
                            .map(String::valueOf)
                            .collect(Collectors.joining()));
        }
        return totalOutputValue;
    }

    private String intersection(final String str1, final String str2) {
        return Sets.intersection(getSetOfCharacters(str1), getSetOfCharacters(str2))
                .stream()
                .sorted()
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

    private String union(final String str1, final String str2) {
        return Sets.union(getSetOfCharacters(str1), getSetOfCharacters(str2))
                .stream()
                .sorted()
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

    private Set<Character> getSetOfCharacters(final String str) {
        return str.chars()
                .mapToObj(character -> (char) character)
                .collect(Collectors.toSet());
    }

    @Getter
    @AllArgsConstructor
    static class Display {
        final List<String> signalPatterns;
        final List<String> outputValue;
    }
}
