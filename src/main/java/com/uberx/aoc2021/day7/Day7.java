package com.uberx.aoc2021.day7;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day7 {
    public static List<Integer> getCrabHorizontalPositions(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> Arrays.stream(line.split(","))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList()))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public int totalFuelCost1(final List<Integer> crabHorizontalPositions) {
        final List<Integer> sortedCrabHorizontalPositions = crabHorizontalPositions.stream()
                .sorted()
                .collect(Collectors.toList());
        final IntStream medians;
        if (sortedCrabHorizontalPositions.size() % 2 == 1) {
            final int median = sortedCrabHorizontalPositions.get(sortedCrabHorizontalPositions.size() / 2);
            medians = IntStream.range(median, median + 1);
        } else {
            final int median = (sortedCrabHorizontalPositions.get(sortedCrabHorizontalPositions.size() / 2 - 1)
                    + sortedCrabHorizontalPositions.get(sortedCrabHorizontalPositions.size() / 2))
                    / 2;
            medians = IntStream.rangeClosed(median, median + 1);
        }
        return medians.mapToObj(cost -> crabHorizontalPositions.stream()
                        .map(horizontalPosition -> Math.abs(horizontalPosition - cost))
                        .reduce(0, Integer::sum))
                .min(Integer::compareTo)
                .orElse(-1);
    }

    /* Part 2 */
    public int totalFuelCost2(final List<Integer> crabHorizontalPositions) {
        final float mean = crabHorizontalPositions.stream().reduce(0, Integer::sum)
                / (float) crabHorizontalPositions.size();
        final IntStream means = IntStream.rangeClosed((int) Math.floor(mean), (int) Math.ceil(mean));
        return means.mapToObj(cost -> crabHorizontalPositions.stream()
                        .map(horizontalPosition -> Math.abs(horizontalPosition - cost)
                                * (Math.abs(horizontalPosition - cost) + 1)
                                / 2)
                        .reduce(0, Integer::sum))
                .min(Integer::compareTo)
                .orElse(-1);
    }
}
