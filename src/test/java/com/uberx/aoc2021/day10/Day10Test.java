package com.uberx.aoc2021.day10;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day10Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-----------------------");
        System.out.print("\nDay 10 - Syntax Scoring");
        System.out.print("\n-----------------------\n\n");
    }

    @Test
    public void testDay10Part1() throws IOException {
        final List<String> chunks = Day10.getChunks("day10");

        final Day10 solver = new Day10();
        assertEquals(
                Integer.valueOf(442131),
                Utils.<Integer>runWithExecutionTime1(() -> solver.totalSyntaxErrorScore(chunks)));
    }

    @Test
    public void testDay10Part2() throws IOException {
        final List<String> chunks = Day10.getChunks("day10");

        final Day10 solver = new Day10();
        assertEquals(
                Long.valueOf(3646451424L),
                Utils.<Long>runWithExecutionTime2(() -> solver.middleCompletionStringScore(chunks)));
    }
}
