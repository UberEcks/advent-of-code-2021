package com.uberx.aoc2021.day24;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day24Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n------------------------------");
        System.out.print("\nDay 24 - Arithmetic Logic Unit");
        System.out.print("\n------------------------------\n\n");
    }

    @Test
    public void testDay24Part1() throws IOException {
        final List<Day24.InstructionSet> instructionSets = Day24.getInstructionSets("day24");

        final Day24 solver = new Day24();
        assertEquals(
                Long.valueOf(51983999947999L),
                Utils.<Long>runWithExecutionTime1(() -> solver.largestValidModelNumber(instructionSets)));
    }

    @Test
    public void testDay24Part2() throws IOException {
        final List<Day24.InstructionSet> instructionSets = Day24.getInstructionSets("day24");

        final Day24 solver = new Day24();
        assertEquals(
                Long.valueOf(11211791111365L),
                Utils.<Long>runWithExecutionTime2(() -> solver.smallestValidModelNumber(instructionSets)));
    }
}
