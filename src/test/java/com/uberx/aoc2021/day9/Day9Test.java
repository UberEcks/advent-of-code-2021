package com.uberx.aoc2021.day9;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day9Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 9 - Smoke Basin");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay9Part1() throws IOException {
        final int[][] heightmap = Day9.getHeightmap("Day9");

        final Day9 solver = new Day9();
        assertEquals(
                Integer.valueOf(423),
                Utils.<Integer>runWithExecutionTime1(() -> solver.sumOfRiskLevelsOfLowPoints(heightmap)));
    }

    @Test
    public void testDay9Part2() throws IOException {
        final int[][] heightmap = Day9.getHeightmap("Day9");

        final Day9 solver = new Day9();
        assertEquals(
                Integer.valueOf(1198704),
                Utils.<Integer>runWithExecutionTime2(() -> solver.productOfThe3LargestBasinSizes(heightmap)));
    }
}
