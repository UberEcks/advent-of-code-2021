package com.uberx.aoc2021;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class DayTemplate {
    public static List<?> getEntries(final String filename) throws IOException {
        return IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
    }

    /* Part 1 */
    public int part1(final List<?> entries) {
        return 1;
    }

    /* Part 2 */
    public int part2(final List<?> entries) {
        return 2;
    }
}
