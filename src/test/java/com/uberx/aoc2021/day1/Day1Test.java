package com.uberx.aoc2021.day1;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day1Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 1 - Sonar Sweep");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay1Part1() throws IOException {
        final List<Integer> entries = Day1.getDepthEntries("day1");

        final Day1 solver = new Day1();
        assertEquals(
                Integer.valueOf(1616),
                Utils.<Integer>runWithExecutionTime1(() -> solver.numDepthIncreases(entries)));
    }

    @Test
    public void testDay1Part2() throws IOException {
        final List<Integer> entries = Day1.getDepthEntries("day1");

        final Day1 solver = new Day1();
        assertEquals(
                Integer.valueOf(1645),
                Utils.<Integer>runWithExecutionTime2(() -> solver.numSlidingWindowDepthIncreases(entries)));
    }


}
