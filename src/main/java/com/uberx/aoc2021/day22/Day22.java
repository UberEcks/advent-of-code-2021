package com.uberx.aoc2021.day22;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day22 {
    public static List<Cuboid> getCuboids(final String filename) throws IOException {
        final Pattern pattern = Pattern.compile(
                "(on|off) x=(-?\\d+)\\.\\.(-?\\d+),y=(-?\\d+)\\.\\.(-?\\d+),z=(-?\\d+)\\.\\.(-?\\d+)");
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final Matcher matcher = pattern.matcher(line);
                    if (!matcher.matches()) {
                        throw new IllegalStateException();
                    }
                    return new Cuboid(
                            "on".equals(matcher.group(1)),
                            Point3D.of(
                                    Integer.parseInt(matcher.group(2)),
                                    Integer.parseInt(matcher.group(4)),
                                    Integer.parseInt(matcher.group(6))),
                            Point3D.of(
                                    Integer.parseInt(matcher.group(3)),
                                    Integer.parseInt(matcher.group(5)),
                                    Integer.parseInt(matcher.group(7))));
                })
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public long numCubesOnAfterInitializationProcedure(final List<Cuboid> cuboids) {
        final List<Cuboid> initializationProcedureSteps = cuboids.stream()
                .filter(cuboid -> cuboid.getFrom().x >= -50 && cuboid.getTo().x <= 50
                        && cuboid.getFrom().y >= -50 && cuboid.getTo().y <= 50
                        && cuboid.getFrom().z >= -50 && cuboid.getTo().z <= 50)
                .collect(Collectors.toList());

        return numCubesOnAfterRebootSteps(initializationProcedureSteps);
    }

    /* Part 2 */
    public long numCubesOnAfterRebootSteps(final List<Cuboid> cuboids) {
        final List<Cuboid> subCuboids = executeRebootSteps(cuboids);
        return subCuboids.stream()
                .map(subCuboid -> (subCuboid.isOn() ? 1L : -1L)
                        * (subCuboid.getTo().x - subCuboid.getFrom().x + 1)
                        * (subCuboid.getTo().y - subCuboid.getFrom().y + 1)
                        * (subCuboid.getTo().z - subCuboid.getFrom().z + 1))
                .reduce(0L, Long::sum);
    }

    private List<Cuboid> executeRebootSteps(final List<Cuboid> cuboids) {
        final List<Cuboid> subCuboids = new ArrayList<>();
        for (final Cuboid cuboid : cuboids) {
            final List<Cuboid> subIntersectionCuboids = new ArrayList<>();
            if (cuboid.isOn()) {
                subIntersectionCuboids.add(cuboid);
            }

            for (final Cuboid subCuboid : subCuboids) {
                final Cuboid intersection = intersection(subCuboid, cuboid, !subCuboid.isOn());
                if (intersection != null) {
                    subIntersectionCuboids.add(intersection);
                }
            }
            subCuboids.addAll(subIntersectionCuboids);
        }
        return subCuboids;
    }

    private Cuboid intersection(final Cuboid cuboid1,
                                final Cuboid cuboid2,
                                boolean on) {
        if (cuboid2.getFrom().x > cuboid1.getTo().x || cuboid2.getTo().x < cuboid1.getFrom().x
                || cuboid2.getFrom().y > cuboid1.getTo().y || cuboid2.getTo().y < cuboid1.getFrom().y
                || cuboid2.getFrom().z > cuboid1.getTo().z || cuboid2.getTo().z < cuboid1.getFrom().z) {
            return null;
        }
        return new Cuboid(on,
                Point3D.of(
                        Math.max(cuboid1.getFrom().x, cuboid2.getFrom().x),
                        Math.max(cuboid1.getFrom().y, cuboid2.getFrom().y),
                        Math.max(cuboid1.getFrom().z, cuboid2.getFrom().z)),
                Point3D.of(
                        Math.min(cuboid1.getTo().x, cuboid2.getTo().x),
                        Math.min(cuboid1.getTo().y, cuboid2.getTo().y),
                        Math.min(cuboid1.getTo().z, cuboid2.getTo().z)));
    }

    @Getter
    @AllArgsConstructor
    static class Cuboid {
        final boolean on;
        final Point3D from;
        final Point3D to;
    }

    @AllArgsConstructor
    static class Point3D {
        public int x;
        public int y;
        public int z;

        static Point3D of(final int x,
                          final int y,
                          final int z) {
            return new Point3D(x, y, z);
        }
    }
}
