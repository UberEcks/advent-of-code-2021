package com.uberx.aoc2021;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class DayTemplateTest {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n------------------------------");
        System.out.print("\nDay Template - Just a template");
        System.out.print("\n------------------------------\n\n");
    }

    @Test
    public void testDayTemplatePart1() throws IOException {
        final List<?> entries = DayTemplate.getEntries("dayTemplate");

        final DayTemplate solver = new DayTemplate();
        assertEquals(
                Integer.valueOf(1),
                Utils.<Integer>runWithExecutionTime1(() -> solver.part1(entries)));
    }

    @Test
    public void testDayTemplatePart2() throws IOException {
        final List<?> entries = DayTemplate.getEntries("dayTemplate");

        final DayTemplate solver = new DayTemplate();
        assertEquals(
                Integer.valueOf(2),
                Utils.<Integer>runWithExecutionTime2(() -> solver.part2(entries)));
    }
}
