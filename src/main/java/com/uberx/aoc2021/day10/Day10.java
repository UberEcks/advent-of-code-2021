package com.uberx.aoc2021.day10;

import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day10 {
    public static final Map<Character, Character> CHARACTER_PAIRS = ImmutableMap.of(
            '(', ')',
            '[', ']',
            '{', '}',
            '<', '>');

    public static List<String> getChunks(final String filename) throws IOException {
        return IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
    }

    /* Part 1 */
    public int totalSyntaxErrorScore(final List<String> chunks) {
        final NavigationSubsystemResult navigationSubsystemResult = getNavigationSubsystemResult(chunks);
        return navigationSubsystemResult.getWrongClosingCharacters()
                .stream()
                .map(character -> {
                    if (character == ')') {
                        return 3;
                    } else if (character == ']') {
                        return 57;
                    } else if (character == '}') {
                        return 1197;
                    }
                    return 25137;
                })
                .reduce(0, Integer::sum);
    }

    private NavigationSubsystemResult getNavigationSubsystemResult(final List<String> chunks) {
        final List<ProcessedChunk> processedChunks = new ArrayList<>();
        final List<Character> wrongClosingCharacters = new ArrayList<>();
        for (final String chunk : chunks) {
            final List<Character> remainingOpenCharacters = new ArrayList<>();
            boolean valid = true;
            for (int i = 0; i < chunk.length() && valid; i++) {
                final char character = chunk.charAt(i);
                if (character == '(' || character == '[' || character == '{' || character == '<') {
                    remainingOpenCharacters.add(character);
                } else {
                    if (!Character.valueOf(character)
                            .equals(CHARACTER_PAIRS.get(
                                    remainingOpenCharacters.remove(remainingOpenCharacters.size() - 1)))) {
                        wrongClosingCharacters.add(character);
                        valid = false;
                    }
                }
            }
            if (valid) {
                processedChunks.add(new ProcessedChunk(chunk, remainingOpenCharacters));
            }
        }
        return new NavigationSubsystemResult(processedChunks, wrongClosingCharacters);
    }

    /* Part 2 */
    public long middleCompletionStringScore(final List<String> chunks) {
        final NavigationSubsystemResult navigationSubsystemResult = getNavigationSubsystemResult(chunks);
        final List<Long> completionStringScores = navigationSubsystemResult.getProcessedChunks()
                .stream()
                .map(processedChunk -> IntStream.range(0, processedChunk.getRemainingOpenCharacters().size())
                        .mapToObj(i -> processedChunk.getRemainingOpenCharacters()
                                .get(processedChunk.getRemainingOpenCharacters().size() - 1 - i))
                        .map(CHARACTER_PAIRS::get)
                        .map(closingCharacter -> {
                            if (closingCharacter == ')') {
                                return 1L;
                            } else if (closingCharacter == ']') {
                                return 2L;
                            } else if (closingCharacter == '}') {
                                return 3L;
                            }
                            return 4L;
                        })
                        .reduce(0L, (acc, curr) -> acc * 5 + curr))
                .collect(Collectors.toList());
        for (final String chunk : chunks) {
            final List<Character> openCharacters = new ArrayList<>();
            boolean valid = true;
            for (int i = 0; i < chunk.length() && valid; i++) {
                final char character = chunk.charAt(i);
                if (character == '(' || character == '[' || character == '{' || character == '<') {
                    openCharacters.add(character);
                } else {
                    if (character != CHARACTER_PAIRS.get(openCharacters.remove(openCharacters.size() - 1))) {
                        valid = false;
                    }
                }
            }
            if (valid) {
                completionStringScores.add(
                        IntStream.range(0, openCharacters.size())
                                .mapToObj(i -> openCharacters.get(openCharacters.size() - 1 - i))
                                .map(CHARACTER_PAIRS::get)
                                .map(closingCharacter -> {
                                    if (closingCharacter == ')') {
                                        return 1L;
                                    } else if (closingCharacter == ']') {
                                        return 2L;
                                    } else if (closingCharacter == '}') {
                                        return 3L;
                                    }
                                    return 4L;
                                })
                                .reduce(0L, (acc, curr) -> acc * 5 + curr));
            }
        }
        return completionStringScores.stream()
                .sorted()
                .collect(Collectors.toList())
                .get(completionStringScores.size() / 2);
    }

    @Getter
    @AllArgsConstructor
    static class NavigationSubsystemResult {
        final List<ProcessedChunk> processedChunks;
        final List<Character> wrongClosingCharacters;
    }

    @Getter
    @AllArgsConstructor
    static class ProcessedChunk {
        final String validChunk;
        final List<Character> remainingOpenCharacters;
    }
}
