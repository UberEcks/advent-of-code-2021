package com.uberx.aoc2021.day9;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day9 {
    public static int[][] getHeightmap(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> line.chars()
                        .map(i -> i - '0')
                        .toArray())
                .toArray(int[][]::new);
    }

    /* Part 1 */
    public int sumOfRiskLevelsOfLowPoints(final int[][] heightmap) {
        final List<Point> lowPoints = getLowPoints(heightmap);
        return lowPoints.stream()
                .map(lowPoint -> heightmap[lowPoint.x][lowPoint.y] + 1)
                .reduce(0, Integer::sum);
    }

    private List<Point> getLowPoints(final int[][] heightmap) {
        final List<Point> lowPoints = new ArrayList<>();
        for (int i = 0; i < heightmap.length; i++) {
            for (int j = 0; j < heightmap[0].length; j++) {
                if ((j == 0 || heightmap[i][j] < heightmap[i][j - 1]) // left
                        && (i == 0 || heightmap[i][j] < heightmap[i - 1][j]) // top
                        && (j == heightmap[0].length - 1 || heightmap[i][j] < heightmap[i][j + 1]) // right
                        && (i == heightmap.length - 1 || heightmap[i][j] < heightmap[i + 1][j])) { // bottom
                    lowPoints.add(new Point(i, j));
                }
            }
        }
        return lowPoints;
    }

    /* Part 2 */
    public int productOfThe3LargestBasinSizes(final int[][] heightmap) {
        final List<Integer> basinSizes = getLowPoints(heightmap).stream()
                .map(lowPoint -> computeBasinSize(heightmap, lowPoint))
                .sorted()
                .collect(Collectors.toList());
        return basinSizes.subList(basinSizes.size() - 3, basinSizes.size())
                .stream()
                .reduce(1, (acc, size) -> acc * size);
    }

    private int computeBasinSize(final int[][] heightmap,
                                 final Point lowPoint) {
        return computeBasinSize(heightmap, new Point(lowPoint.x, lowPoint.y), new HashSet<>());
    }

    private int computeBasinSize(final int[][] heightmap,
                                 final Point currentLocation,
                                 final Set<Point> visited) {
        if (visited.contains(currentLocation)) {
            return 0;
        }
        visited.add(currentLocation);
        if (currentLocation.x < 0
                || currentLocation.x > heightmap.length - 1
                || currentLocation.y < 0
                || currentLocation.y > heightmap[0].length - 1
                || heightmap[currentLocation.x][currentLocation.y] == 9) {
            return 0;
        }
        return 1 + computeBasinSize(heightmap, new Point(currentLocation.x, currentLocation.y - 1), visited) // left
                + computeBasinSize(heightmap, new Point(currentLocation.x - 1, currentLocation.y), visited) // top
                + computeBasinSize(heightmap, new Point(currentLocation.x, currentLocation.y + 1), visited) // right
                + computeBasinSize(heightmap, new Point(currentLocation.x + 1, currentLocation.y), visited); //bottom
    }
}
