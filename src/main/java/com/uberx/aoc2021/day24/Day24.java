package com.uberx.aoc2021.day24;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day24 {
    public static List<InstructionSet> getInstructionSets(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
        final Pattern pattern = Pattern.compile("(inp|add|mul|div|mod|eql) ([wxyz])(\\s[wxyz]|\\s-?\\d+)?");
        final List<InstructionSet> instructionSets = new ArrayList<>();
        List<Instruction> instructions = null;
        for (final String line : lines) {
            final Matcher matcher = pattern.matcher(line);
            if (!matcher.matches()) {
                throw new IllegalStateException();
            }
            final Instruction.Operation operation = Instruction.Operation.fromValue(matcher.group(1));
            final String operand1 = matcher.group(2);
            final String operand2;
            if (operation != Instruction.Operation.INP) {
                operand2 = matcher.group(3).trim();
            } else {
                instructions = new ArrayList<>();
                instructionSets.add(new InstructionSet(instructions));
                operand2 = null;
            }
            Objects.requireNonNull(instructions)
                    .add(new Instruction(operation, operand1, operand2));
        }
        return instructionSets;
    }

    /* Part 1 */
    public long largestValidModelNumber(final List<InstructionSet> instructionSets) {
        return findModelNumber(
                instructionSets,
                () -> IntStream.range(1, 10)
                        .boxed()
                        .collect(Collectors.toList()));
    }

    private long findModelNumber(final List<InstructionSet> instructionSets,
                                 final Supplier<List<Integer>> inputSupplier) {
        final List<Integer> variableValuesForDivZ = getVariableValuesForInstructionDivZ(instructionSets);
        final List<Long> maxZValues = IntStream.range(0, variableValuesForDivZ.size())
                .mapToObj(idx -> 0L)
                .collect(Collectors.toList());
        for (int i = variableValuesForDivZ.size() - 1; i >= 0; i--) {
            if (i == variableValuesForDivZ.size() - 1) {
                maxZValues.set(i, variableValuesForDivZ.get(i).longValue());
            } else {
                maxZValues.set(i, variableValuesForDivZ.get(i) * maxZValues.get(i + 1));
            }
        }
        final Deque<State> dfs = new LinkedList<>();
        dfs.addFirst(new State(new ALU(), -1));
        final Map<State, ALU> cache = new HashMap<>();
        long modelNumber = -1;
        while (!dfs.isEmpty()) {
            final State currState = dfs.pop();
            if (currState.getInstructionSetIdx() == instructionSets.size() - 1) {
                if (currState.getAlu().getRegister("z") == 0) {
                    final StringBuilder modelNumberBuilder = new StringBuilder();
                    State modelNumberState = currState;
                    while (modelNumberState.getParent() != null) {
                        modelNumberBuilder.insert(0, modelNumberState.getInput());
                        modelNumberState = modelNumberState.getParent();
                    }
                    modelNumber = Long.parseLong(modelNumberBuilder.toString());
                    break;
                }
                continue;
            }

            inputSupplier.get()
                    .forEach(input -> {
                        final int instructionSetIdxToApply = currState.getInstructionSetIdx() + 1;
                        final State cacheState = new State(
                                currState.getAlu(), instructionSetIdxToApply, input, currState.parent);
                        final ALU newAlu;
                        if (cache.containsKey(cacheState)) {
                            newAlu = cache.get(cacheState);
                        } else {
                            newAlu = currState.getAlu()
                                    .executeInstructions(
                                            instructionSets.get(instructionSetIdxToApply).getInstructions(),
                                            input);
                            cache.put(cacheState, newAlu);
                        }
                        final State newState = new State(newAlu, instructionSetIdxToApply, input, currState);
                        if (newAlu.getRegister("z") < maxZValues.get(instructionSetIdxToApply)) {
                            dfs.addFirst(newState);
                        }
                    });
        }
        return modelNumber;
    }

    private List<Integer> getVariableValuesForInstructionDivZ(final List<InstructionSet> instructionSets) {
        return instructionSets.stream()
                .map(instructionSet -> instructionSet.getInstructions().get(4))
                .map(Instruction::getOperand2)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    /* Part 2 */
    public long smallestValidModelNumber(final List<InstructionSet> instructionSets) {
        return findModelNumber(
                instructionSets,
                () -> IntStream.range(1, 10)
                        .map(i -> 10 - i)
                        .boxed()
                        .collect(Collectors.toList()));
    }

    @Getter
    @ToString
    @EqualsAndHashCode(exclude = "parent")
    static class State {
        final ALU alu;
        final int instructionSetIdx;
        Integer input;
        State parent;

        public State(final ALU alu, final int instructionSetIdx) {
            this(alu, instructionSetIdx, null, null);
        }

        public State(final ALU alu, final int instructionSetIdx, final State parent) {
            this(alu, instructionSetIdx, null, parent);
        }

        public State(final ALU alu, final int instructionSetIdx, final Integer input) {
            this(alu, instructionSetIdx, input, null);
        }

        public State(final ALU alu, final int instructionSetIdx, final Integer input, final State parent) {
            this.alu = alu;
            this.instructionSetIdx = instructionSetIdx;
            this.input = input;
            this.parent = parent;
        }
    }

    @ToString
    @RequiredArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(exclude = "w")
    static class ALU {
        @Getter
        final List<Long> registers;
        Long w;

        public ALU() {
            this(Lists.newArrayList(0L, 0L, 0L));
        }

        public ALU cloneALU() {
            return new ALU(new ArrayList<>(registers));
        }

        public Long getRegister(final String name) {
            return registers.get(name.charAt(0) - 'x');
        }

        public void setRegister(final String name,
                                final long value) {
            if ("w".equals(name)) {
                w = value;
                return;
            }
            registers.set(name.charAt(0) - 'x', value);
        }

        public ALU executeInstructions(final List<Instruction> instructions,
                                       final long input) {
            final ALU alu = this.cloneALU();
            final List<Long> registers = alu.getRegisters();
            for (final Instruction instruction : instructions) {
                final String operand1 = instruction.getOperand1();
                final Long operand2 = getOperand2(registers, input, instruction.getOperand2());
                switch (instruction.getOperation()) {
                    case INP:
                        alu.setRegister(operand1, input);
                        break;
                    case ADD:
                        alu.setRegister(operand1, alu.getRegister(operand1) + operand2);
                        break;
                    case MUL:
                        alu.setRegister(operand1, alu.getRegister(operand1) * operand2);
                        break;
                    case DIV:
                        alu.setRegister(operand1, alu.getRegister(operand1) / operand2);
                        break;
                    case MOD:
                        alu.setRegister(operand1, alu.getRegister(operand1) % operand2);
                        break;
                    case EQL:
                        alu.setRegister(operand1, Objects.equals(alu.getRegister(operand1), operand2) ? 1L : 0L);
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
            return alu;
        }

        private Long getOperand2(final List<Long> registers,
                                 final long input,
                                 final String operand2) {
            if (operand2 == null) {
                return null;
            }
            final char maybeRegister = operand2.charAt(0);
            if (Character.isAlphabetic(maybeRegister)) {
                if (maybeRegister == 'w') {
                    return input;
                }
                return registers.get(maybeRegister - 'x');
            }
            return Long.parseLong(operand2);
        }
    }

    @Getter
    @AllArgsConstructor
    static class InstructionSet {
        final List<Instruction> instructions;
    }

    @Getter
    @AllArgsConstructor
    static class Instruction {
        final Operation operation;
        final String operand1;
        final String operand2;

        enum Operation {
            INP, ADD, MUL, DIV, MOD, EQL;

            private static final Map<String, Operation> OPERATIONS_BY_NAME = Arrays.stream(Operation.values())
                    .collect(
                            Collectors.toMap(
                                    operation -> operation.name().toLowerCase(Locale.ROOT),
                                    Function.identity()));

            public static Operation fromValue(final String value) {
                return OPERATIONS_BY_NAME.get(value);
            }
        }
    }
}
