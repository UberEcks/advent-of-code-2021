package com.uberx.aoc2021.day20;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day20Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 20 - Trench Map");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay20Part1() throws IOException {
        final Day20.Input input = Day20.getInput("day20");

        final Day20 solver = new Day20();
        assertEquals(
                Integer.valueOf(5573),
                Utils.<Integer>runWithExecutionTime1(() -> solver.litPixelsAfterNRounds(input, 2)));
    }

    @Test
    public void testDay20Part2() throws IOException {
        final Day20.Input input = Day20.getInput("day20");

        final Day20 solver = new Day20();
        assertEquals(
                Integer.valueOf(20097),
                Utils.<Integer>runWithExecutionTime2(() -> solver.litPixelsAfterNRounds(input, 50)));
    }
}
