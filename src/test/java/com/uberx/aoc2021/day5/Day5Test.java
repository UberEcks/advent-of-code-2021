package com.uberx.aoc2021.day5;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day5Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n----------------------------");
        System.out.print("\nDay 5 - Hydrothermal Venture");
        System.out.print("\n----------------------------\n\n");
    }

    @Test
    public void testDay5Part1() throws IOException {
        final List<Day5.LineSegment> lineSegments = Day5.getLineSegments("day5");

        final Day5 solver = new Day5();
        assertEquals(
                Long.valueOf(6007),
                Utils.<Long>runWithExecutionTime1(() -> solver.numPointsWithAtLeast2OverlappingLines1(lineSegments)));
    }

    @Test
    public void testDay5Part2() throws IOException {
        final List<Day5.LineSegment> lineSegments = Day5.getLineSegments("day5");

        final Day5 solver = new Day5();
        assertEquals(
                Long.valueOf(19349),
                Utils.<Long>runWithExecutionTime2(() -> solver.numPointsWithAtLeast2OverlappingLines2(lineSegments)));
    }
}
