package com.uberx.aoc2021.day6;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day6Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 6 - Lanternfish");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay6Part1() throws IOException {
        final List<Integer> fishTimers = Day6.getFishTimers("day6");

        final Day6 solver = new Day6();
        assertEquals(
                Long.valueOf(389726),
                Utils.<Long>runWithExecutionTime1(() -> solver.totalFishAfterNDays(fishTimers, 80)));
    }

    @Test
    public void testDay6Part2() throws IOException {
        final List<Integer> fishTimers = Day6.getFishTimers("day6");

        final Day6 solver = new Day6();
        assertEquals(
                Long.valueOf(1743335992042L),
                Utils.<Long>runWithExecutionTime2(() -> solver.totalFishAfterNDays(fishTimers, 256)));
    }
}
