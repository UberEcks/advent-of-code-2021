package com.uberx.aoc2021.day2;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day2Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------");
        System.out.print("\nDay 2 - Dive!");
        System.out.print("\n-------------\n\n");
    }

    @Test
    public void testDay2Part1() throws IOException {
        final List<Day2.Move> moves = Day2.getSubmarineCourse("day2");

        final Day2 solver = new Day2();
        assertEquals(
                Integer.valueOf(1499229),
                Utils.<Integer>runWithExecutionTime1(() -> solver.productOfHorizontalPositionAndDepth1(moves)));
    }

    @Test
    public void testDay2Part2() throws IOException {
        final List<Day2.Move> moves = Day2.getSubmarineCourse("day2");

        final Day2 solver = new Day2();
        assertEquals(
                Integer.valueOf(1340836560),
                Utils.<Integer>runWithExecutionTime2(() -> solver.productOfHorizontalPositionAndDepth2(moves)));
    }
}
