package com.uberx.aoc2021.day11;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day11Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n----------------------");
        System.out.print("\nDay 11 - Dumbo Octopus");
        System.out.print("\n----------------------\n\n");
    }

    @Test
    public void testDay11Part1() throws IOException {
        final Day11.Grid octopusGrid = Day11.getOctopusGrid("day11");

        final Day11 solver = new Day11();
        assertEquals(
                Integer.valueOf(1717),
                Utils.<Integer>runWithExecutionTime1(() -> solver.totalFlashesAfterNSteps(octopusGrid, 100)));
    }

    @Test
    public void testDay11Part2() throws IOException {
        final Day11.Grid octopusGrid = Day11.getOctopusGrid("day11");

        final Day11 solver = new Day11();
        assertEquals(
                Integer.valueOf(476),
                Utils.<Integer>runWithExecutionTime2(() -> solver.firstStepWhenAllOctopusesFlash(octopusGrid)));
    }
}
