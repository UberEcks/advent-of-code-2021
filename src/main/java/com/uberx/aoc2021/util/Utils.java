package com.uberx.aoc2021.util;

import java.util.function.Supplier;

/**
 * @author uberecks
 */
public class Utils {
    public static <T> T runWithExecutionTime1(final Supplier<T> supplier) {
        return runWithExecutionTime(supplier, "Part 1");
    }

    public static <T> T runWithExecutionTime2(final Supplier<T> supplier) {
        return runWithExecutionTime(supplier, "Part 2");
    }

    public static <T> T runWithExecutionTime(final Supplier<T> supplier, final String message) {
        long startTime = System.nanoTime();
        final T result = supplier.get();
        System.out.printf("%s: %s\n", message, result);
        long endTime = System.nanoTime();

        final float ms = (endTime - startTime) / 1_000_000f;
        final String time;
        if (ms > 1) {
            time = String.format("%.2fms", ms);
        } else if (ms < 1000) {
            time = String.format("%.2fµs", ms * 1_000);
        } else {
            time = String.format("%.2fns", ms * 1_000_000);
        }
        System.out.printf("Total execution time: %s\n\n", time);
        return result;
    }
}
