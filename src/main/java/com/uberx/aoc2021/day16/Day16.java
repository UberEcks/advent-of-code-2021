package com.uberx.aoc2021.day16;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author uberecks
 */
public class Day16 {
    public static String getHexPacket(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .get(0);
    }

    /* Part 1 */
    public int sumOfAllPacketVersions(final String hexPacket) {
        final String binaryPacket = hexToBinary(hexPacket);
        final Packet packet = evaluateBinaryPacket(binaryPacket);

        int sumOfPacketVersions = 0;
        final Queue<Packet> bfs = new LinkedList<>();
        bfs.add(packet);
        while (!bfs.isEmpty()) {
            final Packet currPacket = bfs.poll();
            sumOfPacketVersions += currPacket.getVersion();
            bfs.addAll(currPacket.getSubPackets());
        }
        return sumOfPacketVersions;
    }

    private Packet evaluateBinaryPacket(final String binaryPacket) {
        return new Packet(binaryPacket.toCharArray(), new AtomicInteger(0));
    }

    private String hexToBinary(final String hex) {
        return hex.chars()
                .mapToObj(c -> String.valueOf((char) c))
                .map(hexValue -> StringUtils.leftPad(Integer.toBinaryString(Integer.parseInt(hexValue, 16)), 4, '0'))
                .reduce(String::concat)
                .orElseThrow(IllegalStateException::new);
    }

    /* Part 2 */
    public long outermostPacketValue(final String hexPacket) {
        final String binaryPacket = hexToBinary(hexPacket);
        final Packet packet = evaluateBinaryPacket(binaryPacket);
        return packet.getLiteralValue();
    }

    @Getter
    static class Packet {
        final int version;
        final int typeID;
        long literalValue = 0;
        Integer lengthTypeID;
        final List<Packet> subPackets = new ArrayList<>();
        boolean literalValueComputed;

        Packet(final char[] binaryPacket, final AtomicInteger binaryPacketIndex) {
            // version is the next 3 bits
            this.version = Integer.parseInt(String.valueOf(binaryPacket, binaryPacketIndex.get(), 3), 2);
            // type ID is the next 3 bits
            this.typeID = Integer.parseInt(String.valueOf(binaryPacket, binaryPacketIndex.get() + 3, 3), 2);
            binaryPacketIndex.set(binaryPacketIndex.get() + 6);
            parseRemainingPacket(binaryPacket, binaryPacketIndex);
        }

        private void parseRemainingPacket(final char[] binaryPacket,
                                          final AtomicInteger binaryPacketIndex) {
            // literal value
            if (typeID == 4) {
                final StringBuilder literalValue = new StringBuilder();
                // obtain literal value blocks until the last five bits of the literal value section is reached
                boolean done = false;
                while (!done) {
                    if (binaryPacket[binaryPacketIndex.get()] == '0') {
                        done = true;
                    }
                    // collect every 4-bit literal value block
                    literalValue.append(String.valueOf(binaryPacket, binaryPacketIndex.get() + 1, 4));
                    binaryPacketIndex.set(binaryPacketIndex.get() + 5);
                }
                this.literalValue = Long.parseLong(literalValue.toString(), 2);
                this.literalValueComputed = true;
                // throw out leading zeroes
                final int numLeadingZeroes = literalValue.length() % 4 == 0 ? 0 : 4 - literalValue.length() % 4;
                binaryPacketIndex.set(binaryPacketIndex.get() + numLeadingZeroes);
            }
            // operator
            else {
                // length type ID is the next bit
                this.lengthTypeID = Integer.parseInt(String.valueOf(binaryPacket, binaryPacketIndex.get(), 1), 2);
                if (lengthTypeID == 0) {
                    // sub packets bit length is the next 15 bits
                    final int subPacketsBitLength = Integer.parseInt(
                            String.valueOf(binaryPacket, binaryPacketIndex.get() + 1, 15), 2);
                    binaryPacketIndex.set(binaryPacketIndex.get() + 16);

                    final int startIndex = binaryPacketIndex.get();
                    // evaluate sub packets until the bit length is reached
                    while (binaryPacketIndex.get() != startIndex + subPacketsBitLength) {
                        subPackets.add(new Packet(binaryPacket, binaryPacketIndex));
                    }
                } else if (lengthTypeID == 1) {
                    // number of sub packets is the next 11 bits
                    final int numSubPackets = Integer.parseInt(
                            String.valueOf(binaryPacket, binaryPacketIndex.get() + 1, 11), 2);
                    binaryPacketIndex.set(binaryPacketIndex.get() + 12);

                    // evaluate sub packets until the required number of sub packets is reached
                    for (int i = 0; i < numSubPackets; i++) {
                        subPackets.add(new Packet(binaryPacket, binaryPacketIndex));
                    }
                }
            }
        }

        // computer literal value lazily
        long getLiteralValue() {
            if (literalValueComputed) {
                return literalValue;
            }
            if (typeID == 0) { // sum
                literalValue = subPackets.stream()
                        .map(Packet::getLiteralValue)
                        .reduce(Long::sum)
                        .orElseThrow(IllegalStateException::new);
            } else if (typeID == 1) { // product
                literalValue = subPackets.stream()
                        .map(Packet::getLiteralValue)
                        .reduce(1L, (acc, curr) -> acc * curr);
            } else if (typeID == 2) { // min
                literalValue = subPackets.stream()
                        .map(Packet::getLiteralValue)
                        .reduce(Math::min)
                        .orElseThrow(IllegalStateException::new);
            } else if (typeID == 3) { // max
                literalValue = subPackets.stream()
                        .map(Packet::getLiteralValue)
                        .reduce(Math::max)
                        .orElseThrow(IllegalStateException::new);
            } else if (typeID == 5) { // greater than
                literalValue = subPackets.get(0).getLiteralValue() > subPackets.get(1).getLiteralValue() ? 1 : 0;
            } else if (typeID == 6) { // less than
                literalValue = subPackets.get(0).getLiteralValue() < subPackets.get(1).getLiteralValue() ? 1 : 0;
            } else if (typeID == 7) { // less than
                literalValue = subPackets.get(0).getLiteralValue() == subPackets.get(1).getLiteralValue() ? 1 : 0;
            }
            literalValueComputed = true;
            return literalValue;
        }
    }
}
