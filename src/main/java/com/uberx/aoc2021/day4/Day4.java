package com.uberx.aoc2021.day4;

import com.google.common.collect.Sets;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day4 {
    public static Bingo getBingo(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);

        int line = 0;
        // numbers
        final List<Integer> numbers = Arrays.stream(lines.get(line).split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        line += 2; // skip empty line

        // boards
        final List<Board> boards = new ArrayList<>();
        while (line < lines.size()) {
            final List<int[]> intBoard = new ArrayList<>();
            while (line < lines.size() && !lines.get(line).isEmpty()) {
                intBoard.add(Arrays.stream(lines.get(line).trim().split("\\s+"))
                        .mapToInt(Integer::parseInt)
                        .toArray());
                line++;
            }
            boards.add(new Board(intBoard.toArray(new int[0][])));
            line++;
        }

        return new Bingo(numbers, boards);
    }

    /* Part 1 */
    public int productOfWinningNumberAndUnmarkedNumbers1(final Bingo bingo) {
        final List<Integer> numbers = bingo.getNumbers();
        final List<Board> boards = bingo.getBoards();
        for (final Integer number : numbers) {
            for (final Board board : boards) {
                board.mark(number);
                if (board.isWon()) {
                    return number * board.getSumOfUnmarkedNumbers();
                }
            }
        }
        return -1;
    }

    /* Part 2 */
    public int productOfWinningNumberAndUnmarkedNumbers2(final Bingo bingo) {
        final List<Integer> numbers = bingo.getNumbers();
        final List<Board> boards = bingo.getBoards();
        final Set<Integer> boardsStillPlaying = IntStream.range(0, boards.size())
                .boxed()
                .collect(Collectors.toSet());
        for (final Integer number : numbers) {
            for (int i = 0; i < boards.size(); i++) {
                final Board board = boards.get(i);
                board.mark(number);
                if (boardsStillPlaying.contains(i) && board.isWon()) {
                    boardsStillPlaying.remove(i);
                }
                if (boardsStillPlaying.isEmpty()) {
                    return number * board.getSumOfUnmarkedNumbers();
                }
            }
        }
        return -1;
    }

    @Getter
    @AllArgsConstructor
    static class Bingo {
        final List<Integer> numbers;
        final List<Board> boards;
    }

    @Getter
    static class Board {
        final Cell[][] grid;
        final Map<Integer, Cell> gridLookup;
        Set<Integer> markedNumbers = new HashSet<>();
        boolean won = false;

        Board(final int[][] intGrid) {
            grid = new Cell[intGrid.length][intGrid[0].length];
            gridLookup = new HashMap<>();
            for (int row = 0; row < intGrid.length; row++) {
                for (int col = 0; col < intGrid[0].length; col++) {
                    grid[row][col] = new Cell(intGrid[row][col], row, col, false);
                    gridLookup.put(intGrid[row][col], grid[row][col]);
                }
            }
        }

        public void mark(final Integer number) {
            final Cell cell = gridLookup.get(number);
            if (cell != null) {
                cell.setMarked(true);
                markedNumbers.add(number);
                checkWon(cell);
            }
        }

        private void checkWon(final Cell cell) {
            // check row
            boolean rowMarked = true;
            for (int col = 0; col < grid[0].length; col++) {
                if (!grid[cell.getRow()][col].isMarked()) {
                    rowMarked = false;
                    break;
                }
            }
            if (rowMarked) {
                won = true;
                return;
            }

            // check col
            boolean colMarked = true;
            for (final Cell[] row : grid) {
                if (!row[cell.getCol()].isMarked()) {
                    colMarked = false;
                    break;
                }
            }
            if (colMarked) {
                won = true;
            }
        }

        public int getSumOfUnmarkedNumbers() {
            final Set<Integer> unmarkedIntegers = Sets.difference(gridLookup.keySet(), markedNumbers);
            return unmarkedIntegers.stream()
                    .reduce(0, Integer::sum);
        }
    }

    @Getter
    @AllArgsConstructor
    static class Cell {
        final int number;
        final int row;
        final int col;
        @Setter
        boolean marked;
    }
}
