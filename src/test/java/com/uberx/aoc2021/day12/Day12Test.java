package com.uberx.aoc2021.day12;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day12Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n------------------------");
        System.out.print("\nDay 12 - Passage Pathing");
        System.out.print("\n------------------------\n\n");
    }

    @Test
    public void testDay12Part1() throws IOException {
        final List<Day12.Connection> caveConnections = Day12.getCaveConnections("day12");

        final Day12 solver = new Day12();
        assertEquals(
                Integer.valueOf(4413),
                Utils.<Integer>runWithExecutionTime1(() -> solver.totalPaths1(caveConnections)));
    }

    @Test
    public void testDay12Part2() throws IOException {
        final List<Day12.Connection> caveConnections = Day12.getCaveConnections("day12");

        final Day12 solver = new Day12();
        assertEquals(
                Integer.valueOf(118803),
                Utils.<Integer>runWithExecutionTime2(() -> solver.totalPaths2(caveConnections)));
    }
}
