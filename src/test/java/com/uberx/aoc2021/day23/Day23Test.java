package com.uberx.aoc2021.day23;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day23Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-----------------");
        System.out.print("\nDay 23 - Amphipod");
        System.out.print("\n-----------------\n\n");
    }

    @Test
    public void testDay23Part1() throws IOException {
        final Day23.BurrowState burrowState = Day23.getBurrowState("day23_1");

        final Day23 solver = new Day23();
        assertEquals(
                Integer.valueOf(13455),
                Utils.<Integer>runWithExecutionTime1(() -> solver.leastEnergyToOrganizeAmphipods(burrowState)));
    }

    @Test
    public void testDay23Part2() throws IOException {
        final Day23.BurrowState burrowState = Day23.getBurrowState("day23_2");

        final Day23 solver = new Day23();
        assertEquals(
                Integer.valueOf(43567),
                Utils.<Integer>runWithExecutionTime2(() -> solver.leastEnergyToOrganizeAmphipods(burrowState)));
    }
}
