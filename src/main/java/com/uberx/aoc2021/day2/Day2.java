package com.uberx.aoc2021.day2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day2 {
    public static List<Move> getSubmarineCourse(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(input -> {
                    final String[] tokens = input.split(" ");
                    return new Move(Direction.valueOf(tokens[0].toUpperCase(Locale.ROOT)), Integer.parseInt(tokens[1]));
                })
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public int productOfHorizontalPositionAndDepth1(final List<Move> moves) {
        return productOfHorizontalPositionAndDepth(moves, (move) -> move.getDirection().getApplyFunction1());
    }

    /* Part 2 */
    public int productOfHorizontalPositionAndDepth2(final List<Move> moves) {
        return productOfHorizontalPositionAndDepth(moves, (move) -> move.getDirection().getApplyFunction2());
    }

    private int productOfHorizontalPositionAndDepth(
            final List<Move> moves,
            final Function<Move, BiFunction<Position, Integer, Position>> applyFuncFunction) {
        Position position = new Position(0, 0, 0);
        for (final Move move : moves) {
            position = applyFuncFunction.apply(move).apply(position, move.getAmount());
        }
        return position.horizontalPosition * position.getDepth();
    }

    @Getter
    @AllArgsConstructor
    static class Move {
        final Direction direction;
        final int amount;
    }

    @Getter
    @AllArgsConstructor
    static class Position {
        final int horizontalPosition;
        final int depth;
        final int aim;
    }

    @Getter
    @AllArgsConstructor
    enum Direction {
        UP(
                (position, amount) -> new Position(position.horizontalPosition, position.depth - amount, 0),
                (position, amount) -> new Position(position.horizontalPosition, position.depth, position.aim - amount)),
        DOWN(
                (position, amount) -> new Position(position.horizontalPosition, position.depth + amount, 0),
                (position, amount) -> new Position(position.horizontalPosition, position.depth, position.aim + amount)),
        FORWARD(
                (position, amount) -> new Position(position.horizontalPosition + amount, position.depth, 0),
                (position, amount) -> new Position(
                        position.horizontalPosition + amount, position.depth + position.aim * amount, position.aim));

        final BiFunction<Position, Integer, Position> applyFunction1;
        final BiFunction<Position, Integer, Position> applyFunction2;
    }
}
