package com.uberx.aoc2021.day13;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day13Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n----------------------------");
        System.out.print("\nDay 13 - Transparent Origami");
        System.out.print("\n----------------------------\n\n");
    }

    @Test
    public void testDay13Part1() throws IOException {
        final Day13.TransparentPaper transparentPaper = Day13.getTransparentPaper("day13");

        final Day13 solver = new Day13();
        assertEquals(
                Integer.valueOf(837),
                Utils.<Integer>runWithExecutionTime1(() -> solver.visibleDotsAfterFirstFoldInstruction(transparentPaper)));
    }

    @Test
    public void testDay13Part2() throws IOException {
        final Day13.TransparentPaper transparentPaper = Day13.getTransparentPaper("day13");

        final Day13 solver = new Day13();
        assertEquals(
                "\n" +
                        "█ █ █ █   █ █ █     █ █ █ █     █ █     █     █     █ █     █     █   █     █" + "\n" +
                        "█         █     █         █   █     █   █   █     █     █   █     █   █     █" + "\n" +
                        "█ █ █     █     █       █     █         █ █       █         █ █ █ █   █     █" + "\n" +
                        "█         █ █ █       █       █   █ █   █   █     █         █     █   █     █" + "\n" +
                        "█         █         █         █     █   █   █     █     █   █     █   █     █" + "\n" +
                        "█ █ █ █   █         █ █ █ █     █ █ █   █     █     █ █     █     █     █ █  " + "\n",
                Utils.runWithExecutionTime2(() -> "\n" + solver.outputAfterAllFoldInstructions(transparentPaper)));
    }
}
