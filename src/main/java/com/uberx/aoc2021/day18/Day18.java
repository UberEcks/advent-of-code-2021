package com.uberx.aoc2021.day18;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day18 {
    public static List<ListItem> getSnailfishNumbers(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Day18::parseSnailfishNumber)
                .collect(Collectors.toList());
    }

    private static ListItem parseSnailfishNumber(final String snailfishNumber) {
        final Deque<Pair<ListItem, Integer>> stack = new LinkedList<>();
        StringBuilder number = new StringBuilder();
        ListItem currOutermostItem = null;
        for (int i = 0; i < snailfishNumber.length(); i++) {
            final char c = snailfishNumber.charAt(i);
            if (c == '[') {
                int depth = stack.isEmpty() ? 0 : stack.peek().getRight() + 1;
                final ListItem current = stack.isEmpty() ? null : stack.peek().getLeft();
                stack.push(Pair.of(new ListItem(depth, current), depth));
            } else if (Character.isDigit(c)) {
                number.append(c);
            } else if (c == ',' || c == ']') {
                if (number.length() > 0) {
                    int depth = stack.isEmpty() ? 0 : stack.peek().getRight() + 1;
                    final ListItem current = Objects.requireNonNull(stack.peek()).getLeft();
                    current.add(new ListItem(Integer.parseInt(number.toString()), depth, current));
                    number.setLength(0);
                }
                if (c == ']') {
                    currOutermostItem = stack.pop().getLeft();
                    if (!stack.isEmpty()) {
                        stack.peek().getLeft().add(currOutermostItem);
                    }
                }
            }
        }

        if (currOutermostItem == null) {
            return new ListItem(Integer.parseInt(number.toString()), null);
        }
        return currOutermostItem;
    }

    /* Part 1 */
    public int magnitudeOfSumOfSnailfishNumbers(final List<ListItem> snailfishNumbers) {
        ListItem snailfishNumbersSoFar = snailfishNumbers.get(0);
        for (int i = 1; i < snailfishNumbers.size(); i++) {
            snailfishNumbersSoFar = ListItem.merge(snailfishNumbersSoFar, snailfishNumbers.get(i));

            ExplosiveItem explosiveItem;
            SplittableItem splittableItem;
            do {
                final List<ListItem> elements = getIndividualElements(snailfishNumbersSoFar);
                explosiveItem = findExplosion(elements);
                splittableItem = findSplit(elements);
                if (explosiveItem != null && splittableItem != null) {
                    if (explosiveItem.getElementIndex() <= splittableItem.getElementIndex()) {
                        explosiveItem.explode();
                    } else {
                        splittableItem.split();
                    }
                } else if (explosiveItem != null) {
                    explosiveItem.explode();
                } else if (splittableItem != null) {
                    splittableItem.split();
                }
            } while (explosiveItem != null || splittableItem != null);
        }

        return computeMagnitude(snailfishNumbersSoFar);
    }

    private int computeMagnitude(final ListItem listItem) {
        if (listItem.isRegularValue()) {
            return listItem.getRegularValue();
        }
        if (listItem.getItems()
                .stream()
                .allMatch(ListItem::isRegularValue)) {
            return 3 * listItem.getItems().get(0).getRegularValue() + 2 * listItem.getItems().get(1).getRegularValue();
        }
        return 3 * computeMagnitude(listItem.getItems().get(0)) + 2 * computeMagnitude(listItem.getItems().get(1));
    }

    private List<ListItem> getIndividualElements(final ListItem snailfishNumbers) {
        final List<ListItem> elements = new LinkedList<>();

        final Deque<ListItem> stack = new LinkedList<>();
        stack.push(snailfishNumbers);
        while (!stack.isEmpty()) {
            final ListItem curr = stack.pop();
            if (curr.isRegularValue() || curr.getItems()
                    .stream()
                    .allMatch(ListItem::isRegularValue)) {
                elements.add(0, curr);
            } else {
                curr.getItems()
                        .forEach(stack::addFirst);
            }
        }
        return elements;
    }

    private ExplosiveItem findExplosion(final List<ListItem> elements) {
        ListItem pairToExplode = null;
        int elementIndex = -1;
        for (int i = 0; i < elements.size(); i++) {
            final ListItem element = elements.get(i);
            elementIndex = i;
            if (element.isPair() && element.getDepth() > 3) {
                pairToExplode = element;
                break;
            }
        }
        if (pairToExplode == null) {
            return null;
        }

        final ListItem leftRegularItem = elementIndex == 0
                ? null
                : elements.get(elementIndex - 1).getRightmostRegularItem();
        final ListItem rightRegularItem = elementIndex == elements.size() - 1
                ? null
                : elements.get(elementIndex + 1).getLeftmostRegularItem();
        return new ExplosiveItem(pairToExplode, leftRegularItem, rightRegularItem, elementIndex);
    }

    private SplittableItem findSplit(final List<ListItem> elements) {
        ListItem leftmostSplittableRegularNumber = null;
        int elementIndex = -1;
        for (final ListItem currElement : elements) {
            if (currElement.isRegularValue()) {
                elementIndex++;
                if (currElement.getRegularValue() > 9) {
                    leftmostSplittableRegularNumber = currElement;
                    break;
                }
            } else if (currElement.isPair()) {
                elementIndex++;
                if (currElement.getItems().stream().anyMatch(listItem -> listItem.getRegularValue() > 9)) {
                    leftmostSplittableRegularNumber = currElement.getItems()
                            .stream()
                            .filter(listItem -> listItem.getRegularValue() > 9)
                            .findFirst()
                            .orElseThrow(IllegalStateException::new);
                    break;
                }
                elementIndex++;
            }
        }
        if (leftmostSplittableRegularNumber == null) {
            return null;
        }
        return new SplittableItem(leftmostSplittableRegularNumber, elementIndex);
    }

    /* Part 2 */
    public int pairOfSnailfishNumbersWithLargestMagnitude(final Supplier<List<ListItem>> inputSupplier) {
        int maxMagnitude = Integer.MIN_VALUE;
        List<ListItem> listItems = inputSupplier.get();
        for (int i = 0; i < listItems.size(); i++) {
            for (int j = 0; j < listItems.size(); j++) {
                if (i != j) {
                    int magnitude = magnitudeOfSumOfSnailfishNumbers(
                            Lists.newArrayList(listItems.get(i), listItems.get(j)));
                    maxMagnitude = Math.max(maxMagnitude, magnitude);
                }
                listItems = inputSupplier.get();
            }
        }
        return maxMagnitude;
    }

    @Getter
    static class ListItem {
        @Setter
        Integer regularValue;
        List<ListItem> items = new ArrayList<>();
        int depth;
        final ListItem parent;

        ListItem(final int depth,
                 final ListItem parent) {
            this(null, depth, parent);
        }

        ListItem(final Integer regularValue,
                 final int depth,
                 final ListItem parent) {
            this.regularValue = regularValue;
            this.depth = depth;
            this.parent = parent;
        }

        public static ListItem merge(final ListItem listItem1, final ListItem listItem2) {
            incrementDepthExhaustively(listItem1);
            incrementDepthExhaustively(listItem2);
            final ListItem mergedListItem = new ListItem(0, null);
            mergedListItem.add(listItem1, listItem2);
            return mergedListItem;
        }

        private static void incrementDepthExhaustively(final ListItem listItem) {
            final Queue<ListItem> bfs = new LinkedList<>();
            bfs.add(listItem);
            while (!bfs.isEmpty()) {
                final ListItem curr = bfs.poll();
                bfs.addAll(curr.getItems());
                curr.incrementDepth();
            }
        }

        void incrementDepth() {
            depth++;
        }

        void add(final ListItem... listItem) {
            items.addAll(Arrays.asList(listItem));
        }

        boolean isPair() {
            return regularValue == null;
        }

        boolean isRegularValue() {
            return regularValue != null;
        }

        void explode() {
            items.clear();
            regularValue = 0;
        }

        void split() {
            regularValue = null;
        }

        ListItem getRightmostRegularItem() {
            if (isRegularValue()) {
                return this;
            }
            return items.get(items.size() - 1);
        }

        ListItem getLeftmostRegularItem() {
            if (isRegularValue()) {
                return this;
            }
            return items.get(0);
        }

        @Override
        public String toString() {
            if (regularValue != null) {
                return regularValue.toString();
            }
            return items.toString();
        }
    }

    @Getter
    @AllArgsConstructor
    static class ExplosiveItem {
        final ListItem pairToExplode;
        final ListItem leftRegularItem;
        final ListItem rightRegularItem;
        final int elementIndex;

        void explode() {
            if (leftRegularItem != null) {
                leftRegularItem.setRegularValue(
                        leftRegularItem.getRegularValue() + pairToExplode.getItems().get(0).getRegularValue());
            }
            if (rightRegularItem != null) {
                rightRegularItem.setRegularValue(
                        rightRegularItem.getRegularValue() + pairToExplode.getItems().get(1).getRegularValue());
            }
            pairToExplode.explode();
        }
    }

    @Getter
    @AllArgsConstructor
    static class SplittableItem {
        final ListItem regularNumberToSplit;
        final int elementIndex;

        void split() {
            final int halfRegularNumber = regularNumberToSplit.getRegularValue() / 2;
            regularNumberToSplit.add(
                    new ListItem(
                            halfRegularNumber,
                            regularNumberToSplit.getDepth() + 1,
                            regularNumberToSplit),
                    new ListItem(
                            regularNumberToSplit.getRegularValue() - halfRegularNumber,
                            regularNumberToSplit.getDepth() + 1,
                            regularNumberToSplit));
            regularNumberToSplit.split();
        }
    }
}
