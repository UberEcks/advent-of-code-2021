package com.uberx.aoc2021.day16;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day16Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-----------------------");
        System.out.print("\nDay 16 - Packet Decoder");
        System.out.print("\n-----------------------\n\n");
    }

    @Test
    public void testDay16Part1() throws IOException {
        final String packet = Day16.getHexPacket("day16");

        final Day16 solver = new Day16();
        assertEquals(
                Integer.valueOf(897),
                Utils.<Integer>runWithExecutionTime1(() -> solver.sumOfAllPacketVersions(packet)));
    }

    @Test
    public void testDay16Part2() throws IOException {
        final String packet = Day16.getHexPacket("day16");

        final Day16 solver = new Day16();
        assertEquals(
                Long.valueOf(9485076995911L),
                Utils.<Long>runWithExecutionTime2(() -> solver.outermostPacketValue(packet)));
    }
}
