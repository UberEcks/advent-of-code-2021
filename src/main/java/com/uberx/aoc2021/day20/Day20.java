package com.uberx.aoc2021.day20;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day20 {
    public static Input getInput(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
        final String imageEnhancementAlgorithm = lines.get(0);

        final int[][] imageGrid = lines.subList(2, lines.size())
                .stream()
                .map(line -> line.chars()
                        .toArray())
                .toArray(int[][]::new);
        final Set<Point> litPointsInImage = new LinkedHashSet<>();
        for (int i = 0; i < imageGrid.length; i++) {
            for (int j = 0; j < imageGrid[0].length; j++) {
                if (imageGrid[i][j] == '#') {
                    litPointsInImage.add(new Point(i, j));
                }
            }
        }
        return new Input(imageEnhancementAlgorithm, litPointsInImage);
    }

    /* Part 1 & 2 */
    public int litPixelsAfterNRounds(final Input input,
                                     final int rounds) {
        final String imageEnhancementAlgorithm = input.getImageEnhancementAlgorithm();
        final Set<Point> litPointsInImage = input.getLitPointsInImage();

        Set<Point> currLitPoints = new LinkedHashSet<>(litPointsInImage);
        for (int round = 1; round <= rounds; round++) {
            final Pair<Point, Point> minAndMaxCoordinates = getMinAndMaxCoordinates(currLitPoints);
            final Point minCoordinate = minAndMaxCoordinates.getLeft();
            final Point maxCoordinate = minAndMaxCoordinates.getRight();
            final Set<Point> newLitPoints = new HashSet<>();
            for (int i = minCoordinate.x - 1; i <= maxCoordinate.x + 1; i++) {
                for (int j = minCoordinate.y - 1; j <= maxCoordinate.y + 1; j++) {
                    final StringBuilder neighborBinaryRepresentation = new StringBuilder();
                    for (int k = -1; k <= 1; k++) {
                        for (int l = -1; l <= 1; l++) {
                            if (isPointLit(
                                    new Point(i + k, j + l),
                                    currLitPoints,
                                    minCoordinate,
                                    maxCoordinate,
                                    imageEnhancementAlgorithm.charAt(0),
                                    round)) {
                                neighborBinaryRepresentation.append("1");
                            } else {
                                neighborBinaryRepresentation.append("0");
                            }
                        }
                    }
                    final int imageEnhancementAlgorithmIndex = Integer.parseInt(
                            neighborBinaryRepresentation.toString(), 2);
                    if (imageEnhancementAlgorithm.charAt(imageEnhancementAlgorithmIndex) == '#') {
                        newLitPoints.add(new Point(i, j));
                    }
                }
            }
            currLitPoints = newLitPoints;
        }
        return currLitPoints.size();
    }

    private boolean isPointLit(final Point point,
                               final Set<Point> litPoints,
                               final Point minCoordinate,
                               final Point maxCoordinate,
                               final char algImprovementImageIndexZero,
                               final int round) {
        if (litPoints.contains(point)) {
            return true;
        }
        if (point.x < minCoordinate.x || point.y < minCoordinate.y
                || point.x > maxCoordinate.x || point.y > maxCoordinate.y) {
            return algImprovementImageIndexZero == '#' && round % 2 == 0;
        }
        return false;
    }

    private Pair<Point, Point> getMinAndMaxCoordinates(final Set<Point> litPointsInImage) {
        final Point min = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        final Point max = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        for (final Point litPoint : litPointsInImage) {
            min.x = Math.min(min.x, litPoint.x);
            min.y = Math.min(min.y, litPoint.y);
            max.x = Math.max(max.x, litPoint.x);
            max.y = Math.max(max.y, litPoint.y);
        }
        return Pair.of(min, max);
    }

    @Getter
    @AllArgsConstructor
    static class Input {
        final String imageEnhancementAlgorithm;
        final Set<Point> litPointsInImage;
    }
}
