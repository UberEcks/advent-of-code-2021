package com.uberx.aoc2021.day3;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day3Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------------");
        System.out.print("\nDay 3 - Binary Diagnostic");
        System.out.print("\n-------------------------\n\n");
    }

    @Test
    public void testDay3Part1() throws IOException {
        final List<String> entries = Day3.getEntries("day3");

        final Day3 solver = new Day3();
        assertEquals(
                Integer.valueOf(1092896),
                Utils.<Integer>runWithExecutionTime1(() -> solver.powerConsumption(entries)));
    }

    @Test
    public void testDay3Part2() throws IOException {
        final List<String> entries = Day3.getEntries("day3");

        final Day3 solver = new Day3();
        assertEquals(
                Integer.valueOf(4672151),
                Utils.<Integer>runWithExecutionTime2(() -> solver.lifeSupportRating(entries)));
    }
}
