package com.uberx.aoc2021.day5;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day5 {
    public static List<LineSegment> getLineSegments(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final String[] tokens = line.split(" -> ");
                    final String[] fromTokens = tokens[0].split(",");
                    final String[] toTokens = tokens[1].split(",");
                    return new LineSegment(
                            new Point(Integer.parseInt(fromTokens[0]), Integer.parseInt(fromTokens[1])),
                            new Point(Integer.parseInt(toTokens[0]), Integer.parseInt(toTokens[1])));
                })
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public long numPointsWithAtLeast2OverlappingLines1(final List<LineSegment> lineSegments) {
        return numPointsWithAtLeast2OverlappingLines(lineSegments, LineSegment::getPointsCovered1);
    }

    /* Part 2 */
    public long numPointsWithAtLeast2OverlappingLines2(final List<LineSegment> lineSegments) {
        return numPointsWithAtLeast2OverlappingLines(lineSegments, LineSegment::getPointsCovered2);
    }

    private long numPointsWithAtLeast2OverlappingLines(final List<LineSegment> lineSegments,
                                                       final Function<LineSegment, List<Point>> countFunction) {
        final Map<Point, Long> pointCounts = lineSegments.stream()
                .map(countFunction)
                .flatMap(List::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return pointCounts.entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .count();
    }

    @Getter
    @AllArgsConstructor
    static class LineSegment {
        final Point from;
        final Point to;

        public List<Point> getPointsCovered1() {
            if (from.x == to.x) {
                return IntStream.rangeClosed(Math.min(from.y, to.y), Math.max(from.y, to.y))
                        .mapToObj(y -> new Point(from.x, y))
                        .collect(Collectors.toList());
            } else if (from.y == to.y) {
                return IntStream.rangeClosed(Math.min(from.x, to.x), Math.max(from.x, to.x))
                        .mapToObj(x -> new Point(x, from.y))
                        .collect(Collectors.toList());
            }
            return new ArrayList<>();
        }

        public List<Point> getPointsCovered2() {
            if (from.x != to.x && from.y != to.y) {
                return IntStream.rangeClosed(0, Math.abs(to.x - from.x))
                        .mapToObj(shift -> new Point(
                                from.x + (to.x > from.x ? shift : -shift),
                                from.y + (to.y > from.y ? shift : -shift)))
                        .collect(Collectors.toList());
            }
            return getPointsCovered1();
        }
    }
}
