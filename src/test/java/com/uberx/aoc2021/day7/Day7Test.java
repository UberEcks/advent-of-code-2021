package com.uberx.aoc2021.day7;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day7Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------------------");
        System.out.print("\nDay 7 - The Treachery of Whales");
        System.out.print("\n-------------------------------\n\n");
    }

    @Test
    public void testDay7Part1() throws IOException {
        final List<Integer> crabHorizontalPositions = Day7.getCrabHorizontalPositions("day7");

        final Day7 solver = new Day7();
        assertEquals(
                Integer.valueOf(341534),
                Utils.<Integer>runWithExecutionTime1(() -> solver.totalFuelCost1(crabHorizontalPositions)));
    }

    @Test
    public void testDay7Part2() throws IOException {
        final List<Integer> crabHorizontalPositions = Day7.getCrabHorizontalPositions("day7");

        final Day7 solver = new Day7();
        assertEquals(
                Integer.valueOf(93397632),
                Utils.<Integer>runWithExecutionTime2(() -> solver.totalFuelCost2(crabHorizontalPositions)));
    }
}
