package com.uberx.aoc2021.day12;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day12 {
    private static final String START_CAVE = "start";
    private static final String END_CAVE = "end";

    public static List<Connection> getCaveConnections(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    final String[] tokens = line.split("-");
                    return new Connection(tokens[0], tokens[1]);
                })
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public int totalPaths1(final List<Connection> caveConnections) {
        final List<List<String>> paths = findPaths(caveConnections, this::meetsCriteriaForLowercaseCavesInPath1);
        return paths.size();
    }

    private List<List<String>> findPaths(final List<Connection> caveConnections,
                                         final BiFunction<List<String>, String, Boolean> lowercaseCavesCriteria) {
        final Map<String, List<String>> caveNeighbors = new HashMap<>();
        for (final Connection caveConnection : caveConnections) {
            if (!END_CAVE.equals(caveConnection.getFrom())) {
                if (!caveNeighbors.containsKey(caveConnection.getFrom())) {
                    caveNeighbors.put(caveConnection.getFrom(), new ArrayList<>());
                }
                caveNeighbors.get(caveConnection.getFrom()).add(caveConnection.getTo());
            }
            if (!END_CAVE.equals(caveConnection.getTo())) {
                if (!caveNeighbors.containsKey(caveConnection.getTo())) {
                    caveNeighbors.put(caveConnection.getTo(), new ArrayList<>());
                }
                caveNeighbors.get(caveConnection.getTo()).add(caveConnection.getFrom());
            }
        }

        final List<List<String>> solvedPaths = new ArrayList<>();
        final Set<List<String>> visitedPaths = new HashSet<>();
        final Queue<List<String>> bfs = new LinkedList<>();
        bfs.add(Lists.newArrayList(START_CAVE));
        while (!bfs.isEmpty()) {
            final List<String> currPath = bfs.poll();
            visitedPaths.add(currPath);
            final String currCave = currPath.get(currPath.size() - 1);
            if (currCave.equals(END_CAVE)) {
                solvedPaths.add(currPath);
            } else {
                final List<List<String>> newPaths = caveNeighbors.get(currCave)
                        .stream()
                        .filter(neighbor -> {
                            if (neighbor.chars()
                                    .allMatch(c -> Character.isUpperCase((char) c))) {
                                return true;
                            } else {
                                return lowercaseCavesCriteria.apply(currPath, neighbor);
                            }
                        })
                        .filter(neighbor -> !neighbor.equals(START_CAVE))
                        .map(validNeighbor -> {
                            final List<String> neighborWithExistingPath = Lists.newArrayList(currPath);
                            neighborWithExistingPath.add(validNeighbor);
                            return neighborWithExistingPath;
                        })
                        .filter(newPath -> !visitedPaths.contains(newPath))
                        .collect(Collectors.toList());
                bfs.addAll(newPaths);
            }
        }
        return solvedPaths;
    }

    /* Part 2 */
    public int totalPaths2(final List<Connection> caveConnections) {
        final List<List<String>> paths = findPaths(caveConnections, this::meetsCriteriaForLowercaseCavesInPath2);
        return paths.size();
    }

    private boolean meetsCriteriaForLowercaseCavesInPath1(final List<String> currPath, final String neighbor) {
        return !currPath.contains(neighbor);
    }

    private boolean meetsCriteriaForLowercaseCavesInPath2(final List<String> currPath, final String neighbor) {
        final Map<String, Long> lowercaseCaveCounts = currPath.stream()
                .filter(path -> path.chars()
                        .allMatch(c -> Character.isLowerCase((char) c)))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        lowercaseCaveCounts.compute(
                neighbor,
                (cave, count) -> count == null ? 1 : count + 1);
        final Map<Long, Long> lowercaseCaveCountOfCounts = lowercaseCaveCounts.values()
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return lowercaseCaveCountOfCounts.getOrDefault(2L, 0L) < 2
                && lowercaseCaveCountOfCounts.getOrDefault(3L, 0L) == 0;
    }

    @Getter
    @ToString
    @AllArgsConstructor
    static class Connection {
        final String from;
        final String to;
    }
}
