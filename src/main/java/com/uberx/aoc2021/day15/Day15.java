package com.uberx.aoc2021.day15;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day15 {
    public static int[][] getCaveRiskLevelsGrid(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> line.chars()
                        .map(i -> i - '0')
                        .toArray())
                .toArray(int[][]::new);
    }

    /* Part 1 */
    public int pathWithLowestTotalRisk(final int[][] riskLevelsGrid) {
        final Cave cave = new Cave(riskLevelsGrid);
        return computePathWithLowestRiskLevel(cave);
    }

    private int computePathWithLowestRiskLevel(final Cave cave) {
        final GraphNode[][] caveGrid = cave.getCaveGrid();
        caveGrid[0][0].setDistanceFromStart(0);

        final PriorityQueue<GraphNode> unvisitedNodes = new PriorityQueue<>();
        final Set<GraphNode> visitedNodes = new HashSet<>();
        final GraphNode startNode = caveGrid[0][0];
        final GraphNode endNode = caveGrid[caveGrid.length - 1][caveGrid[0].length - 1];
        GraphNode currNode = startNode;
        currNode.setDistanceFromStart(0);
        while (currNode != endNode) {
            final Map<GraphNode, Integer> neighbors = currNode.getNeighbors();
            for (final Map.Entry<GraphNode, Integer> neighborEntry :
                    neighbors.entrySet()
                            .stream()
                            .filter(entry -> !visitedNodes.contains(entry.getKey()))
                            .collect(Collectors.toSet())) {
                final GraphNode neighborNode = neighborEntry.getKey();
                final int neighborDistanceFromCurrNode = neighborEntry.getValue();
                final int newNeighborDistanceFromStart = currNode.getDistanceFromStart() + neighborDistanceFromCurrNode;
                if (neighborNode.getDistanceFromStart() > newNeighborDistanceFromStart) {
                    neighborNode.setDistanceFromStart(newNeighborDistanceFromStart);
                    unvisitedNodes.add(neighborNode);
                }
            }
            visitedNodes.add(currNode);
            currNode = unvisitedNodes.poll();
            if (currNode == null) {
                throw new IllegalStateException();
            }
        }

        return endNode.getDistanceFromStart();
    }

    /* Part 2 */
    public int pathWithLowestTotalRiskOnExtendedGrid(final int[][] riskLevelsGrid,
                                                     final Point tileSize) {
        final int[][] extendedRiskLevelsGrid = extendRiskLevelsGrid(riskLevelsGrid, tileSize);
        final Cave cave = new Cave(extendedRiskLevelsGrid);
        return computePathWithLowestRiskLevel(cave);
    }

    private int[][] extendRiskLevelsGrid(final int[][] riskLevelsGrid,
                                         final Point tileSize) {
        final Point localGridSize = new Point(riskLevelsGrid.length, riskLevelsGrid[0].length);
        final int[][] extendedRiskLevelsGrid = new int[localGridSize.x * tileSize.x][localGridSize.y * tileSize.y];
        for (int row = 0; row < extendedRiskLevelsGrid.length; row++) {
            for (int col = 0; col < extendedRiskLevelsGrid[0].length; col++) {
                final Point coordinates = new Point(row, col);
                final Point tile = getTile(coordinates, localGridSize);
                if (tile.x == 0 && tile.y == 0) {
                    extendedRiskLevelsGrid[row][col] = riskLevelsGrid[row][col];
                } else {
                    Point leftOrTopTile;
                    if (tile.y - 1 >= 0) { // left tile exists
                        leftOrTopTile = new Point(tile.x, tile.y - 1);

                    } else if (tile.x - 1 >= 0) { // top tile exists
                        leftOrTopTile = new Point(tile.x - 1, tile.y);
                    } else {
                        throw new IllegalStateException();
                    }
                    final Point coordinatesForLeftOrTopTile = getCoordinatesForTile(
                            coordinates, leftOrTopTile, localGridSize);
                    extendedRiskLevelsGrid[row][col] = extendedRiskLevelsGrid
                            [coordinatesForLeftOrTopTile.x][coordinatesForLeftOrTopTile.y] % 9 + 1;
                }
            }
        }
        return extendedRiskLevelsGrid;
    }

    private Point getTile(final Point coordinates,
                          final Point localGridSize) {
        return new Point(coordinates.x / localGridSize.x, coordinates.y / localGridSize.y);
    }

    private Point getCoordinatesForTile(final Point coordinates,
                                        final Point tile,
                                        final Point localGridSize) {
        final Point localizedCoordinates = new Point(coordinates.x % localGridSize.x, coordinates.y % localGridSize.y);
        return new Point(
                localizedCoordinates.x + (tile.x * localGridSize.x),
                localizedCoordinates.y + (tile.y * localGridSize.y));
    }

    @Getter
    static class Cave {
        final GraphNode[][] caveGrid;
        final int[][] caveRiskLevelsGrid;

        Cave(final int[][] caveRiskLevels) {
            this.caveRiskLevelsGrid = caveRiskLevels;
            this.caveGrid = new GraphNode[caveRiskLevels.length][caveRiskLevels[0].length];
            for (int row = 0; row < caveRiskLevels.length; row++) {
                for (int col = 0; col < caveRiskLevels[0].length; col++) {
                    this.caveGrid[row][col] = new GraphNode(new Point(row, col));
                }
            }
            computeNeighbors();
        }

        private void computeNeighbors() {
            for (int row = 0; row < caveRiskLevelsGrid.length; row++) {
                for (int col = 0; col < caveRiskLevelsGrid[0].length; col++) {
                    final Map<GraphNode, Integer> neighbors = new HashMap<>();
                    final GraphNode currNode = caveGrid[row][col];
                    if (col - 1 >= 0) { // left
                        neighbors.put(caveGrid[row][col - 1], caveRiskLevelsGrid[row][col - 1]);
                    }
                    if (row - 1 >= 0) { // top
                        neighbors.put(caveGrid[row - 1][col], caveRiskLevelsGrid[row - 1][col]);
                    }
                    if (col + 1 < caveRiskLevelsGrid[0].length) { // right
                        neighbors.put(caveGrid[row][col + 1], caveRiskLevelsGrid[row][col + 1]);
                    }
                    if (row + 1 < caveRiskLevelsGrid.length) { // bottom
                        neighbors.put(caveGrid[row + 1][col], caveRiskLevelsGrid[row + 1][col]);
                    }
                    currNode.setNeighbors(neighbors);
                }
            }
        }
    }

    @ToString(of = {"location", "distanceFromStart"})
    @Getter
    @RequiredArgsConstructor
    static class GraphNode implements Comparable<GraphNode> {
        final Point location;
        @Setter
        int distanceFromStart = Integer.MAX_VALUE;
        @Setter
        Map<GraphNode, Integer> neighbors;

        @Override
        public int compareTo(final GraphNode o) {
            return Integer.compare(distanceFromStart, o.getDistanceFromStart());
        }
    }
}