package com.uberx.aoc2021.day21;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day21Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 21 - Dirac Dice");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay21Part1() throws IOException {
        final Day21.DiracDiceGame diracDiceGame = Day21.getEntries("day21");

        final Day21 solver = new Day21();
        assertEquals(
                Integer.valueOf(1196172),
                Utils.<Integer>runWithExecutionTime1(() -> solver.productOfLosingScoreAndNumDiceRolls(diracDiceGame)));
    }

    @Test
    public void testDay21Part2() throws IOException {
        final Day21.DiracDiceGame diracDiceGame = Day21.getEntries("day21");

        final Day21 solver = new Day21();
        assertEquals(
                Long.valueOf(106768284484217L),
                Utils.<Long>runWithExecutionTime2(() -> solver.greaterNumOfUniversesWithWins(diracDiceGame)));
    }
}
