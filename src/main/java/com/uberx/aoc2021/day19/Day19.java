package com.uberx.aoc2021.day19;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day19 {
    private static final Function<Point3D, List<Point3D>> ROTATIONS_FUNCTION =
            (p) -> Lists.newArrayList(
                    Point3D.of(p.x, p.y, p.z), Point3D.of(p.x, -p.z, p.y),
                    Point3D.of(p.x, -p.y, -p.z), Point3D.of(p.x, p.z, -p.y));
    @SuppressWarnings("SuspiciousNameCombination")
    private static final Function<Point3D, List<Point3D>> DIRECTIONS_FUNCTION =
            (p) -> Lists.newArrayList(
                    Point3D.of(p.x, p.y, p.z), Point3D.of(-p.y, p.x, p.z), Point3D.of(-p.z, p.y, p.x));
    private static final Function<Point3D, List<Point3D>> DIRECTION_FLIP_FUNCTION =
            (p) -> Lists.newArrayList(Point3D.of(p.x, p.y, p.z), Point3D.of(-p.x, -p.y, p.z));

    public static List<Scanner> getScanners(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
        final List<Scanner> scanners = new ArrayList<>();
        Set<Point3D> beaconLocations = new LinkedHashSet<>();
        int id = 0;
        for (final String line : lines) {
            if (line.contains(" scanner ")) {
                beaconLocations = new LinkedHashSet<>();
            } else if (!line.isEmpty()) {
                final String[] tokens = line.split(",");
                beaconLocations.add(
                        Point3D.of(
                                Integer.parseInt(tokens[0]),
                                Integer.parseInt(tokens[1]),
                                Integer.parseInt(tokens[2])));
            } else {
                final Scanner scanner = new Scanner(id, beaconLocations);
                id++;
                if (scanners.isEmpty()) {
                    scanner.setLocationRelativeTo(Pair.of(Point3D.of(0, 0, 0), null));
                }
                scanners.add(scanner);
            }
        }
        scanners.add(new Scanner(id, beaconLocations));
        return scanners;
    }

    /* Part 1 */
    public int numBeacons(final List<Scanner> scanners) {
        computeScannerLocations(scanners);
        return scanners.stream()
                .map(Scanner::getBeaconLocations)
                .flatMap(Set::stream)
                .collect(Collectors.toSet())
                .size();
    }

    private void computeScannerLocations(final List<Scanner> scanners) {
        final Queue<Scanner> queue = new LinkedList<>();
        queue.add(scanners.get(0));
        while (!queue.isEmpty()) {
            final Scanner currOrigin = queue.poll();
            for (int i = 1; i < scanners.size(); i++) {
                final Scanner target = scanners.get(i);
                if (currOrigin.getId() != target.getId() && target.getLocationRelativeTo() == null) {
                    if (computeScannerLocationRelativeToOrigin(currOrigin, target)) {
                        queue.add(target);
                    }
                }
            }
        }
    }

    private boolean computeScannerLocationRelativeToOrigin(final Scanner origin,
                                                           final Scanner target) {
        final List<List<Point3D>> targetBeaconLocationsForAllOrientations = target.getBeaconLocations()
                .stream()
                .map(beaconLocation -> DIRECTIONS_FUNCTION.apply(beaconLocation)
                        .stream()
                        .map(DIRECTION_FLIP_FUNCTION)
                        .flatMap(List::stream)
                        .map(ROTATIONS_FUNCTION)
                        .flatMap(List::stream)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        for (int i = 0; i < 24; i++) {
            final int orientationType = i;
            final Pair<Set<Point3D>, Point3D> commonBeacons = getTranslatedTargetBeacons(
                    origin,
                    targetBeaconLocationsForAllOrientations.stream()
                            .map(orientedLocations -> orientedLocations.get(orientationType))
                            .collect(Collectors.toCollection(LinkedHashSet::new)));
            if (commonBeacons != null) {
                target.setLocationRelativeTo(Pair.of(commonBeacons.getRight(), origin));
                target.getBeaconLocations().clear();
                target.getBeaconLocations().addAll(commonBeacons.getLeft());
                return true;
            }
        }
        return false;
    }

    private Pair<Set<Point3D>, Point3D> getTranslatedTargetBeacons(final Scanner origin,
                                                                   final Set<Point3D> reorientedTargetBeacons) {
        final Set<Point3D> originBeacons = origin.getBeaconLocations();
        for (final Point3D beaconToCompare : originBeacons) {
            for (final Point3D reorientedTargetBeacon : reorientedTargetBeacons) {
                final Point3D translation = getTranslation(reorientedTargetBeacon, beaconToCompare);
                final Set<Point3D> translatedTargetBeacons = reorientedTargetBeacons.stream()
                        .map(beacon -> beacon.translate(translation))
                        .collect(Collectors.toSet());
                final Set<Point3D> commonBeacons = Sets.intersection(originBeacons, translatedTargetBeacons);
                if (commonBeacons.size() >= 12) {
                    return Pair.of(translatedTargetBeacons, translation);
                }
            }
        }
        return null;
    }

    private Point3D getTranslation(final Point3D from,
                                   final Point3D to) {
        return Point3D.of(to.x - from.x, to.y - from.y, to.z - from.z);
    }

    /* Part 2 */
    public int maxManhattanDistanceBetweenScanners(final List<Scanner> scanners) {
        computeScannerLocations(scanners);

        int maxManhattanDistance = Integer.MIN_VALUE;
        for (int i = 0; i < scanners.size() - 1; i++) {
            for (int j = i + 1; j < scanners.size(); j++) {
                int currManhattanDistance = Point3D.manhattanDistance(
                        scanners.get(i).getLocationRelativeTo().getLeft(),
                        scanners.get(j).getLocationRelativeTo().getLeft());
                if (currManhattanDistance > maxManhattanDistance) {
                    maxManhattanDistance = currManhattanDistance;
                }
            }
        }
        return maxManhattanDistance;
    }

    @Getter
    @EqualsAndHashCode(of = "id")
    @RequiredArgsConstructor
    static class Scanner {
        @Setter
        Pair<Point3D, Scanner> locationRelativeTo;
        final int id;
        final Set<Point3D> beaconLocations;
    }

    @EqualsAndHashCode
    @AllArgsConstructor
    static class Point3D {
        public int x;
        public int y;
        public int z;

        static Point3D of(final int x,
                          final int y,
                          final int z) {
            return new Point3D(x, y, z);
        }

        public Point3D translate(final Point3D point) {
            return Point3D.of(x + point.x, y + point.y, z + point.z);
        }

        @Override
        public String toString() {
            return String.format("(%d,%d,%d)", x, y, z);
        }

        public static int manhattanDistance(final Point3D p1,
                                            final Point3D p2) {
            return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y) + Math.abs(p1.z - p2.z);
        }
    }
}
