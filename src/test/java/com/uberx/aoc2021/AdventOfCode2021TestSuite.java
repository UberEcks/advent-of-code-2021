package com.uberx.aoc2021;

import com.uberx.aoc2021.day1.Day1Test;
import com.uberx.aoc2021.day10.Day10Test;
import com.uberx.aoc2021.day11.Day11Test;
import com.uberx.aoc2021.day12.Day12Test;
import com.uberx.aoc2021.day13.Day13Test;
import com.uberx.aoc2021.day14.Day14Test;
import com.uberx.aoc2021.day15.Day15Test;
import com.uberx.aoc2021.day16.Day16Test;
import com.uberx.aoc2021.day17.Day17Test;
import com.uberx.aoc2021.day18.Day18Test;
import com.uberx.aoc2021.day19.Day19Test;
import com.uberx.aoc2021.day2.Day2Test;
import com.uberx.aoc2021.day20.Day20Test;
import com.uberx.aoc2021.day21.Day21Test;
import com.uberx.aoc2021.day22.Day22Test;
import com.uberx.aoc2021.day23.Day23Test;
import com.uberx.aoc2021.day24.Day24Test;
import com.uberx.aoc2021.day25.Day25Test;
import com.uberx.aoc2021.day3.Day3Test;
import com.uberx.aoc2021.day4.Day4Test;
import com.uberx.aoc2021.day5.Day5Test;
import com.uberx.aoc2021.day6.Day6Test;
import com.uberx.aoc2021.day7.Day7Test;
import com.uberx.aoc2021.day8.Day8Test;
import com.uberx.aoc2021.day9.Day9Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author uberecks
 */
@SuiteClasses({
        Day1Test.class, Day2Test.class, Day3Test.class, Day4Test.class, Day5Test.class,
        Day6Test.class, Day7Test.class, Day8Test.class, Day9Test.class, Day10Test.class,
        Day11Test.class, Day12Test.class, Day13Test.class, Day14Test.class, Day15Test.class,
        Day16Test.class, Day17Test.class, Day18Test.class, Day19Test.class, Day20Test.class,
        Day21Test.class, Day22Test.class, Day23Test.class, Day24Test.class, Day25Test.class})
@RunWith(Suite.class)
public class AdventOfCode2021TestSuite {
}
