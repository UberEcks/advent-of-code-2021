package com.uberx.aoc2021.day18;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day18Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n------------------");
        System.out.print("\nDay 18 - Snailfish");
        System.out.print("\n------------------\n\n");
    }

    @Test
    public void testDay18Part1() throws IOException {
        final List<Day18.ListItem> snailfishNumbers = Day18.getSnailfishNumbers("day18");

        final Day18 solver = new Day18();
        assertEquals(
                Integer.valueOf(4433),
                Utils.<Integer>runWithExecutionTime1(() -> solver.magnitudeOfSumOfSnailfishNumbers(snailfishNumbers)));
    }

    @Test
    public void testDay18Part2() {
        final Day18 solver = new Day18();
        assertEquals(
                Integer.valueOf(4559),
                Utils.<Integer>runWithExecutionTime2(
                        () -> solver.pairOfSnailfishNumbersWithLargestMagnitude(() -> {
                            try {
                                return Day18.getSnailfishNumbers("day18");
                            } catch (final IOException e) {
                                throw new RuntimeException(e);
                            }
                        })));
    }
}
