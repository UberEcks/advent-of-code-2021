package com.uberx.aoc2021.day25;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day25Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n---------------------");
        System.out.print("\nDay 25 - Sea Cucumber");
        System.out.print("\n---------------------\n\n");
    }

    @Test
    public void testDay25Part1() throws IOException {
        final Day25.Seafloor seafloor = Day25.getSeafloor("day25");

        final Day25 solver = new Day25();
        assertEquals(
                Integer.valueOf(486),
                Utils.<Integer>runWithExecutionTime1(() -> solver.part1(seafloor)));
    }
}
