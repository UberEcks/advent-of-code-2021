package com.uberx.aoc2021.day4;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day4Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 4 - Giant Squid");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay4Part1() throws IOException {
        final Day4.Bingo bingo = Day4.getBingo("day4");

        final Day4 solver = new Day4();
        assertEquals(
                Integer.valueOf(25023),
                Utils.<Integer>runWithExecutionTime1(() -> solver.productOfWinningNumberAndUnmarkedNumbers1(bingo)));
    }

    @Test
    public void testDay4Part2() throws IOException {
        final Day4.Bingo bingo = Day4.getBingo("day4");

        final Day4 solver = new Day4();
        assertEquals(
                Integer.valueOf(2634),
                Utils.<Integer>runWithExecutionTime2(() -> solver.productOfWinningNumberAndUnmarkedNumbers2(bingo)));
    }
}
