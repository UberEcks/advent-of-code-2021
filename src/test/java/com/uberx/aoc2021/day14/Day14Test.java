package com.uberx.aoc2021.day14;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day14Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n--------------------------------");
        System.out.print("\nDay 14 - Extended Polymerization");
        System.out.print("\n--------------------------------\n\n");
    }

    @Test
    public void testDay14Part1() throws IOException {
        final Day14.Polymer polymer = Day14.getPolymer("day14");

        final Day14 solver = new Day14();
        assertEquals(
                Long.valueOf(3831),
                Utils.<Long>runWithExecutionTime1(
                        () -> solver.differenceOfMostCommonCharacterAndLeastCommonCharacterAfterNSteps(polymer, 10)));
    }

    @Test
    public void testDay14Part2() throws IOException {
        final Day14.Polymer polymer = Day14.getPolymer("day14");

        final Day14 solver = new Day14();
        assertEquals(
                Long.valueOf(5725739914282L),
                Utils.<Long>runWithExecutionTime2(
                        () -> solver.differenceOfMostCommonCharacterAndLeastCommonCharacterAfterNSteps(polymer, 40)));
    }
}
