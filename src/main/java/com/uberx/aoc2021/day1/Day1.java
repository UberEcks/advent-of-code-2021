package com.uberx.aoc2021.day1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day1 {
    public static List<Integer> getDepthEntries(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    /* Part 1 */
    public int numDepthIncreases(final List<Integer> depthEntries) {
        int increases = 0;
        for (int i = 1; i < depthEntries.size(); i++) {
            if (depthEntries.get(i) > depthEntries.get(i - 1)) {
                increases++;
            }
        }
        return increases;
    }

    /* Part 2 */
    public int numSlidingWindowDepthIncreases(final List<Integer> depthEntries) {
        int increases = 0;
        int currSum = depthEntries.get(0) + depthEntries.get(1) + depthEntries.get(2);
        for (int i = 3; i < depthEntries.size(); i++) {
            int nextSum = currSum + depthEntries.get(i) - depthEntries.get(i - 3);
            if (nextSum > currSum) {
                increases++;
            }
            currSum = nextSum;
        }
        return increases;
    }
}
