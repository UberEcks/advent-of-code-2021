package com.uberx.aoc2021.day21;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day21 {
    public static DiracDiceGame getEntries(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
        return new DiracDiceGame(
                new State(Integer.parseInt(lines.get(0).split(":")[1].trim())),
                new State(Integer.parseInt(lines.get(1).split(":")[1].trim())));
    }

    /* Part 1 */
    public int productOfLosingScoreAndNumDiceRolls(final DiracDiceGame diracDiceGame) {
        State player1 = diracDiceGame.getPlayer1();
        State player2 = diracDiceGame.getPlayer2();

        int turn = 0;
        int dice = 0;
        while (true) {
            turn++;
            final List<Integer> diceRolls1 = Lists.newArrayList(
                    (dice + 1 - 1) % 100 + 1,
                    (dice + 2 - 1) % 100 + 1,
                    (dice + 3 - 1) % 100 + 1);
            final int sumOfDiceRolls1 = diceRolls1.stream()
                    .reduce(0, Integer::sum);

            final List<Integer> diceRolls2 = Lists.newArrayList(
                    (dice + 4 - 1) % 100 + 1,
                    (dice + 5 - 1) % 100 + 1,
                    (dice + 6 - 1) % 100 + 1);
            final int sumOfDiceRolls2 = diceRolls2.stream()
                    .reduce(0, Integer::sum);

            dice += 6;

            player1 = player1.playDiceRoll(sumOfDiceRolls1);
            if (player1.getScore() >= 1000) {
                break;
            }

            player2 = player2.playDiceRoll(sumOfDiceRolls2);
            if (player2.getScore() >= 1000) {
                break;
            }
        }

        if (player1.getScore() >= 1000) {
            return player2.getScore() * (6 * turn - 3);
        }

        return player1.getScore() * (6 * turn);
    }

    /* Part 2 */
    public long greaterNumOfUniversesWithWins(final DiracDiceGame diracDiceGame) {
        final Map<Integer, Integer> frequencyByDiceRollSums = ImmutableMap.<Integer, Integer>builder()
                .put(3, 1)
                .put(4, 3)
                .put(5, 6)
                .put(6, 7)
                .put(7, 6)
                .put(8, 3)
                .put(9, 1)
                .build();
        final Pair<Long, Long> wins = play(diracDiceGame, frequencyByDiceRollSums, 1L);
        return Math.max(wins.getLeft(), wins.getRight());
    }

    private Pair<Long, Long> play(final DiracDiceGame diracDiceGame,
                                  final Map<Integer, Integer> frequencyByDiceRollSums,
                                  final long multiplier) {
        if (diracDiceGame.getPlayer1().getScore() >= 21) {
            return Pair.of(multiplier, 0L);
        }
        if (diracDiceGame.getPlayer2().getScore() >= 21) {
            return Pair.of(0L, multiplier);
        }

        final MutablePair<Long, Long> wins = MutablePair.of(0L, 0L);
        IntStream.rangeClosed(3, 9)
                .forEach(diceRollSum -> {
                    final Pair<Long, Long> innerWins = play(
                            diracDiceGame.playDiceRoll(diceRollSum),
                            frequencyByDiceRollSums,
                            multiplier * frequencyByDiceRollSums.get(diceRollSum));
                    wins.setLeft(wins.getLeft() + innerWins.getLeft());
                    wins.setRight(wins.getRight() + innerWins.getRight());
                });
        return wins;
    }

    @Getter
    @AllArgsConstructor
    static class DiracDiceGame {
        final State player1;
        final State player2;
        boolean player1Turn;

        public DiracDiceGame(final State player1,
                             final State player2) {
            this(player1, player2, true);
        }

        public DiracDiceGame playDiceRoll(final int diceRoll) {
            if (player1Turn) {
                return new DiracDiceGame(player1.playDiceRoll(diceRoll), player2.copy(), !player1Turn);
            }
            return new DiracDiceGame(player1.copy(), player2.playDiceRoll(diceRoll), !player1Turn);
        }
    }

    static class State {
        int position;
        @Getter
        int score;

        public State(final int position) {
            this(position, 0);
        }

        public State(final int position,
                     final int score) {
            this.position = position;
            this.score = score;
        }

        State copy() {
            return new State(position, score);
        }

        State playDiceRoll(final int diceRoll) {
            final int position = (this.position + diceRoll - 1) % 10 + 1;
            return new State(position, score + position);
        }
    }
}
