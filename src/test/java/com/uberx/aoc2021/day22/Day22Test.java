package com.uberx.aoc2021.day22;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day22Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-----------------------");
        System.out.print("\nDay 22 - Reactor Reboot");
        System.out.print("\n-----------------------\n\n");
    }

    @Test
    public void testDay22Part1() throws IOException {
        final List<Day22.Cuboid> cuboids = Day22.getCuboids("day22");

        final Day22 solver = new Day22();
        assertEquals(
                Long.valueOf(589411),
                Utils.<Long>runWithExecutionTime1(() -> solver.numCubesOnAfterInitializationProcedure(cuboids)));
    }

    @Test
    public void testDay22Part2() throws IOException {
        final List<Day22.Cuboid> cuboids = Day22.getCuboids("day22");

        final Day22 solver = new Day22();
        assertEquals(
                Long.valueOf(1130514303649907L),
                Utils.<Long>runWithExecutionTime2(() -> solver.numCubesOnAfterRebootSteps(cuboids)));
    }
}
