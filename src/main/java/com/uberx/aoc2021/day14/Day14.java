package com.uberx.aoc2021.day14;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day14 {
    public static Polymer getPolymer(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);

        int line = 0;
        // template
        final String template = lines.get(line);
        line += 2;

        // insertion pairs
        final Map<String, Character> insertionPairs = new HashMap<>();
        for (int i = line; i < lines.size(); i++) {
            final String[] tokens = lines.get(i).split(" -> ");
            insertionPairs.put(tokens[0], tokens[1].charAt(0));
        }

        return new Polymer(template, insertionPairs);
    }

    /* Part 1 & 2 */
    public long differenceOfMostCommonCharacterAndLeastCommonCharacterAfterNSteps(final Polymer polymer, int steps) {
        final Map<String, Long> pairCounts = performSteps(polymer.getTemplate(), polymer.getInsertionPairs(), steps);
        final Map<Character, Long> characterCounts = getCharacterCounts(pairCounts, polymer.getTemplate());

        final long max = characterCounts.values()
                .stream()
                .max(Long::compareTo)
                .orElseThrow(IllegalStateException::new);
        final long min = characterCounts.values()
                .stream()
                .min(Long::compareTo)
                .orElseThrow(IllegalStateException::new);

        return max - min;
    }

    private Map<String, Long> performSteps(final String template,
                                           final Map<String, Character> insertionPairs,
                                           int steps) {
        Map<String, Long> pairCounts = computePairCounts(template);
        for (int i = 0; i < steps; i++) {
            pairCounts = performStep(insertionPairs, pairCounts);
        }

        return pairCounts;
    }

    private Map<String, Long> performStep(final Map<String, Character> insertionPairs,
                                          final Map<String, Long> pairCounts) {
        final Map<String, Long> stepPairCounts = new HashMap<>();
        for (final Map.Entry<String, Long> pairCountEntry : pairCounts.entrySet()) {
            final String pair = pairCountEntry.getKey();
            stepPairCounts.merge(
                    "" + pair.charAt(0) + insertionPairs.get(pair),
                    pairCountEntry.getValue(),
                    Long::sum);
            stepPairCounts.merge(
                    "" + insertionPairs.get(pair) + pair.charAt(1),
                    pairCountEntry.getValue(),
                    Long::sum);
        }
        return stepPairCounts;
    }

    private Map<String, Long> computePairCounts(final String template) {
        final Map<String, Long> pairCounts = new HashMap<>();
        for (int i = 0; i < template.length() - 1; i++) {
            pairCounts.merge("" + template.charAt(i) + template.charAt(i + 1), 1L, Long::sum);
        }
        return pairCounts;
    }

    private Map<Character, Long> getCharacterCounts(final Map<String, Long> pairCounts,
                                                    final String template) {
        final Map<Character, Long> characterCounts = new HashMap<>();

        for (final Map.Entry<String, Long> pairCountEntry : pairCounts.entrySet()) {
            characterCounts.merge(pairCountEntry.getKey().charAt(1), pairCountEntry.getValue(), Long::sum);
        }
        characterCounts.merge(template.charAt(0), 1L, Long::sum);
        return characterCounts;
    }

    @Getter
    @AllArgsConstructor
    static class Polymer {
        final String template;
        final Map<String, Character> insertionPairs;
    }
}
