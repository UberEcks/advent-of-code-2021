package com.uberx.aoc2021.day19;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day19Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-----------------------");
        System.out.print("\nDay 19 - Beacon Scanner");
        System.out.print("\n-----------------------\n\n");
    }

    @Test
    public void testDay19Part1() throws IOException {
        final List<Day19.Scanner> scanners = Day19.getScanners("day19");

        final Day19 solver = new Day19();
        assertEquals(
                Integer.valueOf(436),
                Utils.<Integer>runWithExecutionTime1(() -> solver.numBeacons(scanners)));
    }

    @Test
    public void testDay19Part2() throws IOException {
        final List<Day19.Scanner> scanners = Day19.getScanners("day19");

        final Day19 solver = new Day19();
        assertEquals(
                Integer.valueOf(10918),
                Utils.<Integer>runWithExecutionTime2(() -> solver.maxManhattanDistanceBetweenScanners(scanners)));
    }
}
