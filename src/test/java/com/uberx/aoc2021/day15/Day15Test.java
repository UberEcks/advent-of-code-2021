package com.uberx.aoc2021.day15;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.awt.Point;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day15Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n---------------");
        System.out.print("\nDay 15 - Chiton");
        System.out.print("\n---------------\n\n");
    }

    @Test
    public void testDay15Part1() throws IOException {
        final int[][] caveRiskLevelsGrid = Day15.getCaveRiskLevelsGrid("day15");

        final Day15 solver = new Day15();
        assertEquals(
                Integer.valueOf(503),
                Utils.<Integer>runWithExecutionTime1(() -> solver.pathWithLowestTotalRisk(caveRiskLevelsGrid)));
    }

    @Test
    public void testDay15Part2() throws IOException {
        final int[][] caveRiskLevelsGrid = Day15.getCaveRiskLevelsGrid("day15");

        final Day15 solver = new Day15();
        assertEquals(
                Integer.valueOf(2853),
                Utils.<Integer>runWithExecutionTime2(() -> solver.pathWithLowestTotalRiskOnExtendedGrid(caveRiskLevelsGrid, new Point(5, 5))));
    }
}
