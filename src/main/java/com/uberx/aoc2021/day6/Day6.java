package com.uberx.aoc2021.day6;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day6 {
    public static List<Integer> getFishTimers(final String filename) throws IOException {
        return IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> Arrays.stream(line.split(","))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList()))
                .flatMap(List::stream)
                .collect(Collectors.toList());

    }

    /* Part 1 & 2 */
    public long totalFishAfterNDays(final List<Integer> fishTimers, final int days) {
        Map<Integer, Long> currentFishTimerCounts = fishTimers.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Map<Integer, Long> nextFishTimerCounts;
        for (int i = 0; i < days; i++) {
            nextFishTimerCounts = new HashMap<>();
            for (final Map.Entry<Integer, Long> fishTimer : currentFishTimerCounts.entrySet()) {
                if (fishTimer.getKey() > 0) {
                    nextFishTimerCounts.compute(
                            fishTimer.getKey() - 1,
                            (timer, count) -> (count == null ? fishTimer.getValue() : count + fishTimer.getValue()));
                } else {
                    nextFishTimerCounts.compute(
                            6,
                            (timer, count) -> (count == null ? fishTimer.getValue() : count + fishTimer.getValue()));
                    nextFishTimerCounts.compute(
                            8,
                            (timer, count) -> (count == null ? fishTimer.getValue() : count + fishTimer.getValue()));
                }
            }
            currentFishTimerCounts = nextFishTimerCounts;
        }
        return currentFishTimerCounts.values()
                .stream()
                .reduce(0L, Long::sum);
    }
}
