package com.uberx.aoc2021.day11;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day11 {
    public static Grid getOctopusGrid(final String filename) throws IOException {
        final Cell[][] grid = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> line.chars()
                        .map(i -> i - '0')
                        .mapToObj(Cell::new)
                        .toArray(Cell[]::new))
                .toArray(Cell[][]::new);
        return new Grid(grid);
    }

    /* Part 1 */
    public int totalFlashesAfterNSteps(final Grid octopusGrid,
                                       int steps) {
        int totalFlashes = 0;
        for (int step = 1; step <= steps; step++) {
            final int stepFlashes = simulateStep(octopusGrid, step);
            totalFlashes += stepFlashes;
        }
        return totalFlashes;
    }

    private int simulateStep(final Grid octopusGrid,
                             final int step) {
        final Cell[][] grid = octopusGrid.getGrid();
        for (final Cell[] row : grid) {
            for (int j = 0; j < grid[0].length; j++) {
                row[j].increaseEnergyLevel();
            }
        }

        int stepFlashes = 0;
        int localFlashes;
        do {
            localFlashes = 0;
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid[0].length; col++) {
                    if (grid[row][col].getEnergyLevel() > 9 && octopusGrid.flash(row, col, step)) {
                        localFlashes++;
                    }
                }
            }
            stepFlashes += localFlashes;
        } while (localFlashes != 0);

        return stepFlashes;
    }

    /* Part 2 */
    public int firstStepWhenAllOctopusesFlash(final Grid octopusGrid) {
        int stepFlashes;
        int step = 0;
        do {
            step++;
            stepFlashes = simulateStep(octopusGrid, step);
        } while (stepFlashes != octopusGrid.getGrid().length * octopusGrid.getGrid()[0].length);

        return step;
    }

    @Getter
    @AllArgsConstructor
    static class Grid {
        final Cell[][] grid;

        List<Cell> getNeighbors(final int x,
                                final int y) {
            final List<Cell> neighbors = new ArrayList<>();
            for (int row = -1; row <= 1; row++) {
                for (int col = -1; col <= 1; col++) {
                    if (row != 0 || col != 0) {
                        if (x + row >= 0 && x + row < grid.length
                                && y + col >= 0 && y + col < grid[0].length) {
                            neighbors.add(grid[x + row][y + col]);
                        }
                    }
                }
            }
            return neighbors;
        }

        boolean flash(final int row,
                      final int col,
                      final int step) {
            if (!grid[row][col].hasFlashed(step)) {
                final List<Cell> neighbors = getNeighbors(row, col).stream()
                        .filter(cell -> !cell.hasFlashed(step))
                        .collect(Collectors.toList());
                grid[row][col].flash(step);
                neighbors.forEach(Cell::increaseEnergyLevel);
                return true;
            }
            return false;
        }
    }

    @Getter
    @AllArgsConstructor
    static class Cell {
        int energyLevel;
        final Set<Integer> flashDays = new HashSet<>();

        void increaseEnergyLevel() {
            this.energyLevel = energyLevel + 1;
        }

        void flash(final int step) {
            flashDays.add(step);
            energyLevel = 0;
        }

        boolean hasFlashed(final int step) {
            return flashDays.contains(step);
        }
    }
}
