package com.uberx.aoc2021.day25;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day25 {
    public static Seafloor getSeafloor(final String filename) throws IOException {
        return new Seafloor(
                IOUtils
                        .readLines(
                                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                                StandardCharsets.UTF_8)
                        .stream()
                        .map(line -> line.chars()
                                .mapToObj(i -> (char) i)
                                .toArray(Character[]::new))
                        .toArray(Character[][]::new));
    }

    /* Part 1 */
    public int part1(final Seafloor seafloor) {
        int steps = 0;
        int seaCucumbersMoved;
        do {
            seaCucumbersMoved = executeStep(seafloor);
            steps++;
        } while (seaCucumbersMoved != 0);

        return steps;
    }

    private int executeStep(final Seafloor seafloor) {
        final List<Point> eastFacingSeaCucumbersToMove = seafloor.findEastFacingSeaCucumbersToMove();
        seafloor.moveSeaCucumbers(eastFacingSeaCucumbersToMove);
        final List<Point> southFacingSeaCucumbersToMove = seafloor.findSouthFacingSeaCucumbersToMove();
        seafloor.moveSeaCucumbers(southFacingSeaCucumbersToMove);
        return eastFacingSeaCucumbersToMove.size() + southFacingSeaCucumbersToMove.size();
    }

    @Getter
    @AllArgsConstructor
    static class Seafloor {
        final Character[][] grid;

        public boolean isNeighborEmpty(final Point coordinate) {
            final int row = coordinate.x;
            final int col = coordinate.y;
            if (grid[row][col] == '>') {
                return grid[row][(col + 1) % grid[0].length] == '.';
            } else if (grid[row][col] == 'v') {
                return grid[(row + 1) % grid.length][col] == '.';
            }
            return false;
        }

        public List<Point> findEastFacingSeaCucumbersToMove() {
            return findSeaCucumbersToMove('>');
        }

        public List<Point> findSouthFacingSeaCucumbersToMove() {
            return findSeaCucumbersToMove('v');
        }

        private List<Point> findSeaCucumbersToMove(final Character seaCucumber) {
            final List<Point> seaCucumbersToMove = new ArrayList<>();
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid[0].length; col++) {
                    if (grid[row][col] == seaCucumber && isNeighborEmpty(new Point(row, col))) {
                        seaCucumbersToMove.add(new Point(row, col));
                    }
                }
            }
            return seaCucumbersToMove;
        }

        public void moveSeaCucumbers(final List<Point> cucumberCoordinatesToMove) {
            for (final Point coordinate : cucumberCoordinatesToMove) {
                final int row = coordinate.x;
                final int col = coordinate.y;
                if (grid[row][col] == '>') {
                    grid[row][col] = '.';
                    grid[row][(col + 1) % grid[0].length] = '>';
                } else if (grid[row][col] == 'v') {
                    grid[row][col] = '.';
                    grid[(row + 1) % grid.length][col] = 'v';
                }
            }
        }
    }
}
