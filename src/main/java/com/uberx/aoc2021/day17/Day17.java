package com.uberx.aoc2021.day17;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day17 {
    public static TargetArea getTargetArea(final String filename) throws IOException {
        final List<String> lines = IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
        final String line = lines.get(0).substring("target area: ".length());
        final String[] tokens = line.split(", ");
        final String[] x = tokens[0].split("=")[1].split("\\.\\.");
        final String[] y = tokens[1].split("=")[1].split("\\.\\.");
        return new TargetArea(
                new Point(Integer.parseInt(x[0]), Integer.parseInt(y[0])),
                new Point(Integer.parseInt(x[1]), Integer.parseInt(y[1])));
    }

    /* Part 1 */
    public int highestYPosition(final TargetArea targetArea) {
        final Map<Point, List<Point>> validInitVelocities = getValidInitVelocities(targetArea);
        return validInitVelocities.values()
                .stream()
                .map(path -> path.stream()
                        .map(point -> point.y)
                        .max(Comparator.naturalOrder())
                        .orElseThrow(IllegalStateException::new))
                .max(Comparator.naturalOrder())
                .orElseThrow(IllegalStateException::new);
    }

    private Map<Point, List<Point>> getValidInitVelocities(final TargetArea targetArea) {
        final Map<Point, List<Point>> validInitVelocities = new HashMap<>();

        final int probeXDirection = targetArea.bottomLeft.x > 0 && targetArea.topRight.x > 0 ? 1 : -1;
        final boolean positiveX = probeXDirection == 1;
        int initVelocityY = targetArea.bottomLeft.y;
        while (initVelocityY <= Math.max(Math.abs(targetArea.bottomLeft.y), Math.abs(targetArea.topRight.y))) {
            for (
                    int initVelocityX = probeXDirection;
                    positiveX
                            ? initVelocityX <= targetArea.topRight.x
                            : initVelocityX >= targetArea.bottomLeft.x;
                    initVelocityX += probeXDirection) {
                final Point probe = new Point(0, 0);
                final List<Point> path = new ArrayList<>();
                path.add(probe.getLocation());
                int dx = initVelocityX;
                int dy = initVelocityY;
                // fire and simulate probe movement
                boolean done = false;
                do {
                    probe.translate(dx, dy);
                    path.add(probe.getLocation());
                    if (probe.x >= targetArea.bottomLeft.x && probe.x <= targetArea.topRight.x
                            && probe.y >= targetArea.bottomLeft.y && probe.y <= targetArea.topRight.y) {
                        validInitVelocities.put(new Point(initVelocityX, initVelocityY), path);
                        done = true;
                    }
                    dx = Math.max(0, dx - probeXDirection);
                    dy -= 1;
                } while (isNotAtPointOfNoReturn(positiveX, probe, targetArea) && !done);
            }
            // increase velocity.y
            initVelocityY++;
        }
        return validInitVelocities;
    }

    private boolean isNotAtPointOfNoReturn(final boolean positiveX,
                                           final Point probe,
                                           final TargetArea targetArea) {
        return (positiveX && probe.x <= targetArea.topRight.x && probe.y >= targetArea.bottomLeft.y)
                || (!positiveX && probe.x >= targetArea.bottomLeft.x && probe.y >= targetArea.bottomLeft.y);
    }

    /* Part 2 */
    public int distinctValidInitialVelocities(final TargetArea targetArea) {
        final Map<Point, List<Point>> validInitVelocities = getValidInitVelocities(targetArea);
        return validInitVelocities.size();
    }

    @Getter
    @AllArgsConstructor
    static class TargetArea {
        final Point bottomLeft;
        final Point topRight;
    }
}
