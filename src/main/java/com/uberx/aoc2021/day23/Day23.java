package com.uberx.aoc2021.day23;

import com.google.common.collect.Lists;
import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day23 {
    private static final char WALL = '#';
    private static final char OPEN_SPACE = '.';

    public static BurrowState getBurrowState(final String filename) throws IOException {
        final Cell[][] burrow = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8)
                .stream()
                .map(line -> line.chars()
                        .mapToObj(c -> {
                            char value = (char) c;
                            if (value == ' ') return null;
                            return new Cell(value);
                        })
                        .toArray(Cell[]::new))
                .toArray(Cell[][]::new);
        final AtomicInteger sideRoomIdx = new AtomicInteger(0);
        for (int row = 0; row < burrow.length; row++) {
            for (int col = 0; col < burrow[row].length; col++) {
                final Cell cell = burrow[row][col];
                if (cell != null) {
                    cell.setLocation(new Point(row, col));
                    if (cell.getValue() == WALL) {
                        cell.setType(Cell.Type.WALL);
                    } else {
                        final boolean leftCellIsWall = burrow[row][col - 1].getValue() == WALL;
                        final boolean rightCellIsWall = burrow[row][col + 1].getValue() == WALL;
                        if (leftCellIsWall && rightCellIsWall) {
                            cell.setType(BurrowState.SIDE_ROOMS.get(sideRoomIdx.get() % 4));
                            sideRoomIdx.incrementAndGet();
                        } else {
                            cell.setType(Cell.Type.HALLWAY);
                        }
                    }
                }
            }
        }

        return new BurrowState(burrow);
    }

    /* Part 1 & 2 */
    public int leastEnergyToOrganizeAmphipods(final BurrowState burrowState) {
        final List<BurrowState> winningStates = new ArrayList<>();
        final Deque<BurrowState> dfs = new LinkedList<>();
        final Map<String, Integer> visited = new HashMap<>();
        dfs.addFirst(burrowState);
        while (!dfs.isEmpty()) {
            final BurrowState currState = dfs.pop();
            if (currState.areAllAmphipodsOrganized()) {
                winningStates.add(currState);
                continue;
            } else {
                if (visited.getOrDefault(currState.getState(), Integer.MAX_VALUE) < currState.getEnergy()) {
                    continue;
                }
                visited.compute(
                        currState.getState(),
                        (state, energy) -> energy == null
                                ? currState.getEnergy() : Math.min(currState.getEnergy(), energy));
            }

            currState.getNewPossibleStates()
                    .stream()
                    .filter(newState -> !visited.containsKey(newState.getState())
                            || newState.getEnergy() < visited.get(newState.getState()))
                    .collect(Collectors.toList())
                    .forEach(dfs::addFirst);
        }
        return winningStates.stream()
                .min(Comparator.comparingInt(BurrowState::getEnergy))
                .map(BurrowState::getEnergy)
                .orElseThrow(IllegalStateException::new);
    }

    @Getter
    static class BurrowState {
        static final List<Cell.Type> SIDE_ROOMS = Lists.newArrayList(
                Cell.Type.SIDE_ROOM_A, Cell.Type.SIDE_ROOM_B, Cell.Type.SIDE_ROOM_C, Cell.Type.SIDE_ROOM_D);
        final BurrowState parent;
        final Cell[][] burrow;
        final List<Cell> hallwayCells;
        final Map<Amphipod.Type, List<Cell>> sideRoomsByAmphipodType;
        final Map<Point, Amphipod> amphipodsByLocation;
        final String state;
        @Setter
        int energy = 0;

        public BurrowState(final Cell[][] burrow) {
            this(burrow, null);
        }

        public BurrowState(final Cell[][] burrow,
                           final BurrowState parent) {
            this.parent = parent;
            this.burrow = burrow;
            this.hallwayCells = new ArrayList<>();
            this.sideRoomsByAmphipodType = new HashMap<>();
            this.amphipodsByLocation = new LinkedHashMap<>();
            this.state = toString();
            computeSupplementalFields();
            updateAmphipodsAlreadyMovedOrSolved();
        }

        private void computeSupplementalFields() {
            for (final Cell[] rows : this.burrow) {
                for (final Cell cell : rows) {
                    if (cell != null && cell.getType() == Cell.Type.HALLWAY) {
                        hallwayCells.add(cell);
                    } else if (cell != null && SIDE_ROOMS.contains(cell.getType())) {
                        sideRoomsByAmphipodType.compute(
                                Amphipod.Type.fromCellType(cell.getType()),
                                (k, v) -> {
                                    List<Cell> cells = v;
                                    if (v == null) {
                                        cells = new ArrayList<>();
                                    }
                                    cells.add(cell);
                                    return cells;
                                });
                    }
                    if (cell != null && cell.isAmphipod()) {
                        final Amphipod amphipod = new Amphipod(cell);
                        this.amphipodsByLocation.put(amphipod.getLocation(), amphipod);
                    }
                }
            }
        }

        private void updateAmphipodsAlreadyMovedOrSolved() {
            for (final Map.Entry<Amphipod.Type, List<Cell>> sideRoomEntry : sideRoomsByAmphipodType.entrySet()) {
                final Amphipod.Type amphipodType = sideRoomEntry.getKey();
                final List<Cell> sideRooms = sideRoomEntry.getValue();
                final Map<Point, Amphipod> amphipodByLocation = amphipodsByLocation.values()
                        .stream()
                        .filter(amphipod -> amphipod.getType() == amphipodType)
                        .collect(Collectors.toMap(Amphipod::getLocation, Function.identity()));
                boolean done = false;
                int sideRoomIndex = sideRooms.size() - 1;
                while (sideRoomIndex >= 0 && !done) {
                    if (burrow[sideRooms.get(sideRoomIndex).getLocation().x]
                            [sideRooms.get(sideRoomIndex).getLocation().y]
                            .isAmphipod(amphipodType)) {
                        final Amphipod amphipodAtDestination = amphipodByLocation.get(
                                sideRooms.get(sideRoomIndex).getLocation());
                        amphipodAtDestination.setMovedToHallway(true);
                        amphipodAtDestination.setReachedDestination(true);
                        sideRoomIndex--;
                    } else {
                        done = true;
                    }
                }
            }
            for (final Cell hallwayCell : hallwayCells) {
                if (hallwayCell.isAmphipod()) {
                    final Amphipod amphipod = amphipodsByLocation.get(hallwayCell.getLocation());
                    amphipod.setMovedToHallway(true);
                }
            }
        }

        private Cell[][] cloneBurrow() {
            final Cell[][] clonedBurrow = new Cell[burrow.length][burrow[0].length];
            for (int row = 0; row < burrow.length; row++) {
                for (int col = 0; col < burrow[row].length; col++) {
                    final Cell cell = burrow[row][col];
                    if (cell != null) {
                        clonedBurrow[row][col] = new Cell(cell.getValue(), burrow[row][col].getType());
                        clonedBurrow[row][col].setLocation(new Point(row, col));
                    }
                }
            }
            return clonedBurrow;
        }

        public List<BurrowState> getNewPossibleStates() {
            final List<BurrowState> newPossibleStates = new ArrayList<>();
            for (final Amphipod amphipod : amphipodsByLocation.values()) {
                if (amphipod.isReachedDestination()) {
                    continue;
                }
                if (!amphipod.isMovedToHallway()) {
                    final List<Cell> openHallwaySpaces = hallwayCells.stream()
                            .filter(hallwayCell ->
                                    burrow[hallwayCell.getLocation().x][hallwayCell.getLocation().y].isOpenSpace())
                            .filter(hallwayCell ->
                                    burrow[hallwayCell.getLocation().x + 1][hallwayCell.getLocation().y].isWall())
                            .collect(Collectors.toList());
                    for (final Cell openHallwaySpace : openHallwaySpaces) {
                        final Optional<Integer> path = getPath(
                                burrow[amphipod.getLocation().x][amphipod.getLocation().y],
                                openHallwaySpace);
                        path.ifPresent(steps -> newPossibleStates.add(
                                constructNewPossibleState(amphipod, openHallwaySpace, steps)));
                    }
                } else {
                    final List<Cell> potentialDestinations = sideRoomsByAmphipodType.get(amphipod.getType());
                    final Cell destinationForAmphipod = getDestinationForAmphipod(amphipod, potentialDestinations);
                    if (destinationForAmphipod != null) {
                        final Optional<Integer> path = getPath(
                                burrow[amphipod.getLocation().x][amphipod.getLocation().y],
                                destinationForAmphipod);
                        path.ifPresent(steps -> newPossibleStates.add(
                                constructNewPossibleState(amphipod, destinationForAmphipod, steps)));
                    }
                }
            }
            return newPossibleStates;
        }

        private Cell getDestinationForAmphipod(final Amphipod amphipod,
                                               final List<Cell> potentialDestinations) {
            for (int openSpaceChecks = potentialDestinations.size(); openSpaceChecks >= 1; openSpaceChecks--) {
                if (potentialDestinations.subList(0, openSpaceChecks)
                        .stream()
                        .allMatch(Cell::isOpenSpace)
                        && potentialDestinations.subList(openSpaceChecks, potentialDestinations.size())
                        .stream()
                        .allMatch(cell -> cell.isAmphipod(amphipod.getType()))) {
                    return potentialDestinations.get(openSpaceChecks - 1);
                }
            }
            return null;
        }

        private Optional<Integer> getPath(final Cell from,
                                          final Cell to) {
            final Queue<Pair<Cell, Integer>> bfs = new LinkedList<>();
            final Set<Point> visited = new HashSet<>();
            bfs.add(Pair.of(from, 0));
            while (!bfs.isEmpty()) {
                final Pair<Cell, Integer> currItem = bfs.poll();
                final Cell currCell = currItem.getKey();
                if (currCell.getLocation().equals(to.getLocation())) {
                    return Optional.of(currItem.getValue());
                }
                visited.add(currCell.getLocation());

                final List<Cell> neighbors = new ArrayList<>();
                // top
                final Cell top = burrow[currCell.getLocation().x - 1][currCell.getLocation().y];
                if (isOpenSpaceAndNotVisited(top, visited)) {
                    neighbors.add(top);
                }
                // right
                final Cell right = burrow[currCell.getLocation().x][currCell.getLocation().y + 1];
                if (isOpenSpaceAndNotVisited(right, visited)) {
                    neighbors.add(right);
                }
                // bottom
                final Cell bottom = burrow[currCell.getLocation().x + 1][currCell.getLocation().y];
                if (isOpenSpaceAndNotVisited(bottom, visited)) {
                    neighbors.add(bottom);
                }
                // left
                final Cell left = burrow[currCell.getLocation().x][currCell.getLocation().y - 1];
                if (isOpenSpaceAndNotVisited(left, visited)) {
                    neighbors.add(left);
                }
                bfs.addAll(neighbors.stream()
                        .map(neighbor -> Pair.of(neighbor, currItem.getValue() + 1))
                        .collect(Collectors.toList()));
            }
            return Optional.empty();
        }

        private boolean isOpenSpaceAndNotVisited(final Cell cell,
                                                 final Set<Point> visited) {
            return cell.isOpenSpace() && !visited.contains(cell.getLocation());
        }

        private BurrowState constructNewPossibleState(final Amphipod amphipodToMove,
                                                      final Cell openSpace,
                                                      final int steps) {
            final Cell[][] clonedBurrow = cloneBurrow();
            clonedBurrow[amphipodToMove.getLocation().x][amphipodToMove.getLocation().y].setValue(OPEN_SPACE);
            clonedBurrow[openSpace.getLocation().x][openSpace.getLocation().y]
                    .setValue(amphipodToMove.value());
            final BurrowState newState = new BurrowState(clonedBurrow, this);
            newState.setEnergy(energy + steps * amphipodToMove.getType().getCost());
            return newState;
        }

        public boolean areAllAmphipodsOrganized() {
            for (final Map.Entry<Amphipod.Type, List<Cell>> sideRoomEntry : sideRoomsByAmphipodType.entrySet()) {
                final List<Cell> sideRooms = sideRoomEntry.getValue();
                if (!sideRooms.stream()
                        .allMatch(sideRoom -> burrow[sideRoom.getLocation().x][sideRoom.getLocation().y]
                                .isAmphipod(sideRoomEntry.getKey()))) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            final StringBuilder state = new StringBuilder();
            for (final Cell[] rows : burrow) {
                for (final Cell cell : rows) {
                    if (cell == null) {
                        state.append(' ');
                    } else {
                        state.append(cell.getValue());
                    }
                }
                state.append("\n");
            }
            return state.toString();
        }
    }

    @Getter
    static class Amphipod {
        final Type type;
        final Point location;
        @Setter
        boolean movedToHallway = false;
        @Setter
        boolean reachedDestination = false;

        public Amphipod(final Cell cell) {
            this.type = Type.fromCellValue(cell.getValue());
            this.location = cell.getLocation();
        }

        public char value() {
            return type.name().charAt(0);
        }

        @Getter
        @AllArgsConstructor
        enum Type {
            AMBER(1), BRONZE(10), COPPER(100), DESERT(1000);

            int cost;

            public static Type fromCellType(final Cell.Type cellType) {
                if (cellType == Cell.Type.SIDE_ROOM_A) {
                    return AMBER;
                } else if (cellType == Cell.Type.SIDE_ROOM_B) {
                    return BRONZE;
                } else if (cellType == Cell.Type.SIDE_ROOM_C) {
                    return COPPER;
                } else if (cellType == Cell.Type.SIDE_ROOM_D) {
                    return DESERT;
                }
                throw new IllegalStateException();
            }

            public static Type fromCellValue(final char cellValue) {
                if (cellValue == AMBER.name().charAt(0)) {
                    return AMBER;
                } else if (cellValue == BRONZE.name().charAt(0)) {
                    return BRONZE;
                } else if (cellValue == COPPER.name().charAt(0)) {
                    return COPPER;
                } else if (cellValue == DESERT.name().charAt(0)) {
                    return DESERT;
                }
                return null;
            }
        }
    }

    @Getter
    @Setter
    static class Cell {
        char value;
        Type type;
        Point location;

        public Cell(final char value) {
            this(value, null);
        }

        public Cell(final char value, final Type type) {
            this.value = value;
            this.type = type;
        }

        boolean isOpenSpace() {
            return value == OPEN_SPACE;
        }

        boolean isWall() {
            return type == Type.WALL;
        }

        boolean isAmphipod(final Amphipod.Type amphipodType) {
            return amphipodType.name().charAt(0) == value;
        }

        boolean isAmphipod() {
            final Amphipod.Type amphipodType = Amphipod.Type.fromCellValue(value);
            return amphipodType != null && isAmphipod(amphipodType);
        }

        enum Type {
            WALL, HALLWAY, SIDE_ROOM_A, SIDE_ROOM_B, SIDE_ROOM_C, SIDE_ROOM_D
        }
    }
}
