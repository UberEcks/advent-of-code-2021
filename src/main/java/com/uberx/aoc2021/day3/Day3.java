package com.uberx.aoc2021.day3;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;

/**
 * @author uberecks
 */
public class Day3 {
    public static List<String> getEntries(final String filename) throws IOException {
        return IOUtils.readLines(
                Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                StandardCharsets.UTF_8);
    }

    /* Part 1 */
    public int powerConsumption(final List<String> entries) {
        final Rate rate = computeRate(entries);

        return Integer.parseUnsignedInt(rate.getGamma(), 2) * Integer.parseUnsignedInt(rate.getEpsilon(), 2);
    }

    private Rate computeRate(final List<String> entries) {
        final int[] oneCounts = new int[entries.get(0).length()];
        for (final String entry : entries) {
            char[] characters = entry.toCharArray();
            for (int i = 0; i < characters.length; i++) {
                oneCounts[i] += characters[i] - '0';
            }
        }
        final StringBuilder gammaRate = new StringBuilder();
        final StringBuilder epsilonRate = new StringBuilder();
        for (final int oneCount : oneCounts) {
            if (oneCount >= (entries.size() - oneCount)) {
                gammaRate.append("1");
                epsilonRate.append("0");
            } else {
                gammaRate.append("0");
                epsilonRate.append("1");
            }
        }

        return new Rate(gammaRate.toString(), epsilonRate.toString());
    }

    /* Part 2 */
    public int lifeSupportRating(final List<String> entries) {
        return computeRating(entries, 0, Rating.O2) * computeRating(entries, 0, Rating.CO2);
    }

    private int computeRating(final List<String> entries,
                              final int round,
                              final Rating rating) {
        if (entries.size() == 1) {
            return Integer.parseUnsignedInt(entries.get(0), 2);
        }
        final Rate rate = computeRate(entries);
        final List<String> filteredEntries = entries.stream()
                .filter(entry -> entry.charAt(round) == rating.getBitCriteriaFunction().apply(rate, round))
                .collect(Collectors.toList());
        return computeRating(filteredEntries, round + 1, rating);
    }

    @Getter
    @AllArgsConstructor
    static class Rate {
        final String gamma;
        final String epsilon;
    }

    @Getter
    @AllArgsConstructor
    enum Rating {
        O2((rate, bitPosition) -> rate.getGamma().charAt(bitPosition)),
        CO2((rate, bitPosition) -> rate.getEpsilon().charAt(bitPosition));

        final BiFunction<Rate, Integer, Character> bitCriteriaFunction;
    }
}
