# Advent of Code 2021

https://adventofcode.com/2021/about

## Execute

Run `./mvnw clean verify` to execute all the puzzle challenges

## Results
```
-------------------
Day 1 - Sonar Sweep
-------------------

Part 1: 1616
Total execution time: 509.35µs

Part 2: 1645
Total execution time: 452.02µs


-------------
Day 2 - Dive!
-------------

Part 1: 1499229
Total execution time: 1.15ms

Part 2: 1340836560
Total execution time: 1.08ms


-------------------------
Day 3 - Binary Diagnostic
-------------------------

Part 1: 1092896
Total execution time: 1.13ms

Part 2: 4672151
Total execution time: 5.81ms


-------------------
Day 4 - Giant Squid
-------------------

Part 1: 25023
Total execution time: 7.28ms

Part 2: 2634
Total execution time: 4.05ms


----------------------------
Day 5 - Hydrothermal Venture
----------------------------

Part 1: 6007
Total execution time: 76.66ms

Part 2: 19349
Total execution time: 104.91ms


-------------------
Day 6 - Lanternfish
-------------------

Part 1: 389726
Total execution time: 2.86ms

Part 2: 1743335992042
Total execution time: 1.37ms


-------------------------------
Day 7 - The Treachery of Whales
-------------------------------

Part 1: 341534
Total execution time: 4.53ms

Part 2: 93397632
Total execution time: 2.53ms


----------------------------
Day 8 - Seven Segment Search
----------------------------

Part 1: 525
Total execution time: 2.00ms

Part 2: 1083859
Total execution time: 50.26ms


-------------------
Day 9 - Smoke Basin
-------------------

Part 1: 423
Total execution time: 1.49ms

Part 2: 1198704
Total execution time: 7.12ms


-----------------------
Day 10 - Syntax Scoring
-----------------------

Part 1: 442131
Total execution time: 3.48ms

Part 2: 3646451424
Total execution time: 7.70ms


----------------------
Day 11 - Dumbo Octopus
----------------------

Part 1: 1717
Total execution time: 8.64ms

Part 2: 476
Total execution time: 11.74ms


------------------------
Day 12 - Passage Pathing
------------------------

Part 1: 4413
Total execution time: 61.57ms

Part 2: 118803
Total execution time: 5214.01ms


----------------------------
Day 13 - Transparent Origami
----------------------------

Part 1: 837
Total execution time: 1.33ms

Part 2: 
█ █ █ █   █ █ █     █ █ █ █     █ █     █     █     █ █     █     █   █     █
█         █     █         █   █     █   █   █     █     █   █     █   █     █
█ █ █     █     █       █     █         █ █       █         █ █ █ █   █     █
█         █ █ █       █       █   █ █   █   █     █         █     █   █     █
█         █         █         █     █   █   █     █     █   █     █   █     █
█ █ █ █   █         █ █ █ █     █ █ █   █     █     █ █     █     █     █ █  

Total execution time: 2.50ms


--------------------------------
Day 14 - Extended Polymerization
--------------------------------

Part 1: 3831
Total execution time: 3.80ms

Part 2: 5725739914282
Total execution time: 3.81ms


---------------
Day 15 - Chiton
---------------

Part 1: 503
Total execution time: 32.47ms

Part 2: 2853
Total execution time: 560.29ms


-----------------------
Day 16 - Packet Decoder
-----------------------

Part 1: 897
Total execution time: 10.13ms

Part 2: 9485076995911
Total execution time: 4.97ms


-------------------
Day 17 - Trick Shot
-------------------

Part 1: 11781
Total execution time: 45.87ms

Part 2: 4531
Total execution time: 26.31ms


------------------
Day 18 - Snailfish
------------------

Part 1: 4433
Total execution time: 169.84ms

Part 2: 4559
Total execution time: 3792.59ms


-----------------------
Day 19 - Beacon Scanner
-----------------------

Part 1: 436
Total execution time: 15862.79ms

Part 2: 10918
Total execution time: 15431.09ms


-------------------
Day 20 - Trench Map
-------------------

Part 1: 5573
Total execution time: 31.10ms

Part 2: 20097
Total execution time: 780.35ms


-------------------
Day 21 - Dirac Dice
-------------------

Part 1: 1196172
Total execution time: 1.41ms

Part 2: 106768284484217
Total execution time: 2559.11ms


-----------------------
Day 22 - Reactor Reboot
-----------------------

Part 1: 589411
Total execution time: 11.99ms

Part 2: 1130514303649907
Total execution time: 105.00ms


-----------------
Day 23 - Amphipod
-----------------

Part 1: 13455
Total execution time: 4666.70ms

Part 2: 43567
Total execution time: 3038.90ms


------------------------------
Day 24 - Arithmetic Logic Unit
------------------------------

Part 1: 51983999947999
Total execution time: 42620.44ms

Part 2: 11211791111365
Total execution time: 63.96ms


---------------------
Day 25 - Sea Cucumber
---------------------

Part 1: 486
Total execution time: 134.99ms
```