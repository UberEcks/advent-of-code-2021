package com.uberx.aoc2021.day13;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author uberecks
 */
public class Day13 {
    public static TransparentPaper getTransparentPaper(final String filename) throws IOException {
        final List<String> lines = IOUtils
                .readLines(
                        Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(filename)),
                        StandardCharsets.UTF_8);

        final Set<Point> markedDots = new HashSet<>();
        int line = 0;
        while (line < lines.size() && !lines.get(line).isEmpty()) {
            final String[] tokens = lines.get(line).split(",");
            markedDots.add(new Point(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1])));
            line++;
        }
        line += 1; // skip empty line

        final List<Pair<Character, Integer>> folds = new ArrayList<>();
        while (line < lines.size()) {
            final String[] tokens = lines.get(line).split(" ");
            final String[] foldTokens = tokens[2].split("=");
            folds.add(Pair.of(foldTokens[0].charAt(0), Integer.parseInt(foldTokens[1])));
            line++;
        }
        return new TransparentPaper(markedDots, folds);
    }

    /* Part 1 */
    public int visibleDotsAfterFirstFoldInstruction(final TransparentPaper transparentPaper) {
        return performFolds(transparentPaper.getMarkedPoints(), transparentPaper.getFolds().subList(0, 1))
                .size();
    }

    private Set<Point> performFolds(final Set<Point> initialMarkedPoints,
                                    final List<Pair<Character, Integer>> folds) {
        Set<Point> markedPointsAfterFolds = initialMarkedPoints;
        for (final Pair<Character, Integer> fold : folds) {
            markedPointsAfterFolds = markedPointsAfterFolds.stream()
                    .map(markedPoint -> performFold(markedPoint, fold))
                    .collect(Collectors.toSet());
        }
        return markedPointsAfterFolds;
    }

    private Point performFold(final Point point,
                              final Pair<Character, Integer> fold) {
        if (fold.getLeft() == 'x') {
            return point.x > fold.getRight() ? new Point(2 * fold.getRight() - point.x, point.y) : point;
        } else {
            return point.y > fold.getRight() ? new Point(point.x, 2 * fold.getRight() - point.y) : point;
        }
    }

    /* Part 2 */
    public String outputAfterAllFoldInstructions(final TransparentPaper transparentPaper) {
        final Set<Point> markedPointsAfterFolds = performFolds(
                transparentPaper.getMarkedPoints(), transparentPaper.getFolds());
        return printPaper(markedPointsAfterFolds);
    }

    private String printPaper(final Set<Point> allMarkedPoints) {
        final StringBuilder paper = new StringBuilder();
        final Point maxPoint = allMarkedPoints.stream()
                .reduce(
                        new Point(-1, -1),
                        (acc, curr) -> new Point(Math.max(curr.x, acc.x), Math.max(curr.y, acc.y)));
        for (int y = 0; y <= maxPoint.y; y++) {
            final List<String> row = new ArrayList<>();
            for (int x = 0; x <= maxPoint.x; x++) {
                if (allMarkedPoints.contains(new Point(x, y))) {
                    row.add("█");
                } else {
                    row.add(" ");
                }
            }
            paper.append(String.join(" ", row)).append("\n");
        }
        return paper.toString();
    }

    @Getter
    @AllArgsConstructor
    static class TransparentPaper {
        final Set<Point> markedPoints;
        final List<Pair<Character, Integer>> folds;
    }
}
