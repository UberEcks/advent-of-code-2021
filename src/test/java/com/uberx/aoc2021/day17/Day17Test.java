package com.uberx.aoc2021.day17;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day17Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n-------------------");
        System.out.print("\nDay 17 - Trick Shot");
        System.out.print("\n-------------------\n\n");
    }

    @Test
    public void testDay17Part1() throws IOException {
        final Day17.TargetArea targetArea = Day17.getTargetArea("day17");

        final Day17 solver = new Day17();
        assertEquals(
                Integer.valueOf(11781),
                Utils.<Integer>runWithExecutionTime1(() -> solver.highestYPosition(targetArea)));
    }

    @Test
    public void testDay17Part2() throws IOException {
        final Day17.TargetArea targetArea = Day17.getTargetArea("day17");

        final Day17 solver = new Day17();
        assertEquals(
                Integer.valueOf(4531),
                Utils.<Integer>runWithExecutionTime2(() -> solver.distinctValidInitialVelocities(targetArea)));
    }
}
