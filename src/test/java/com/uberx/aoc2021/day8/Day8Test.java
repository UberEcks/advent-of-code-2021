package com.uberx.aoc2021.day8;

import static org.junit.Assert.assertEquals;

import com.uberx.aoc2021.util.Utils;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author uberecks
 */
public class Day8Test {
    @BeforeClass
    public static void beforeClass() {
        System.out.print("\n----------------------------");
        System.out.print("\nDay 8 - Seven Segment Search");
        System.out.print("\n----------------------------\n\n");
    }

    @Test
    public void testDay8Part1() throws IOException {
        final List<Day8.Display> displays = Day8.getDisplays("day8");

        final Day8 solver = new Day8();
        assertEquals(
                Integer.valueOf(525),
                Utils.<Integer>runWithExecutionTime1(() -> solver.numTimesDigits1Or4Or7Or8Appear(displays)));
    }

    @Test
    public void testDay8Part2() throws IOException {
        final List<Day8.Display> displays = Day8.getDisplays("day8");

        final Day8 solver = new Day8();
        assertEquals(
                Integer.valueOf(1083859),
                Utils.<Integer>runWithExecutionTime2(() -> solver.totalOutputValue(displays)));
    }
}
